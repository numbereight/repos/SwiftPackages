# NumberEight Swift Package

Experimental SPM support for the NumberEight SDK and its components.

## Installing

Add the following to your `Package.swift` dependencies:

```swift
.package(url: "https://gitlab.com/numbereight/repos/NumberEightSPM", from: "3.12.1"),
```

### Using

To use the Audiences SDK:
```swift
import NumberEight
import Audiences
```

If you are using Objective-C, you may need to import the headers directly rather than using `@import`:
```objective-c
#include <NumberEightCompiled/NumberEightCompiled.h>
#include <NEXAudiences.h>
```

## Maintainers
* [Matthew Paletta](matt@numbereight.ai)
* [Chris Watts](chris@numbereight.ai)
