//
//  JSONFormatter.h
//  NumberEightCompiled
//
//  Created by Matthew Paletta on 2021-05-14.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

#pragma once

#import "NEXBaseFormatter.h"

///
/// Implementation to serialize a `NEXSnapshot` as a JSON-string.
///
@interface NEXJSONFormatter : NEXBaseFormatter

///
/// Returns a new instance of a `NEXJSONFormatter`
///
- (instancetype _Nonnull)init;

///
/// Overrides `NEXBaseFormatter` to represent a snapshot as a serialized JSON string.
///
/// @note this is thread safe
/// @return JSON string representation of the snapshot
///
/// @code
/// NEXSnapshotter* snapshotter = [[NEXSnapshotter alloc] initWithDefaults];
///
/// NEXSnapshot* snapshot = [snapshotter takeSnapshot];
///
/// //...
///
/// NEXJSONFormatter* jsonFormatter = [[NEXJSONFormatter alloc] init];
/// NSString* jsonString = [jsonFormatter stringFromSnapshot:snapshot];
///
/// jsonString == @'{"time": "early-morning,weekday", "weather": "cold,snow"}';
/// @endcode
///
- (NSString* _Nullable)stringFromSnapshot:(NEXSnapshot* _Nonnull)snapshot;

@end
