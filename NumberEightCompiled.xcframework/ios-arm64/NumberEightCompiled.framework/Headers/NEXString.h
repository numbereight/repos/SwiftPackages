//
//  NEXString.h
//  NumberEightCompiled
//
//  Created by Matthew Paletta on 2021-05-11.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//
#pragma once

#import "NEString.h"
#import "NEXSensorItem.h"

NS_ASSUME_NONNULL_BEGIN

/**
 * Encapsulates a string.
 */
@interface NEXString : NEXSensorItem

/**
 * Default constructor.
 */
- (instancetype)init;

/**
 * Default constructor.
 */
+ (instancetype)new;

/**
 * Conversion copy constructor.
 * @param value NEString to copy from..
 */
- (instancetype)initWithString:(NEString)value;

/**
 * @param value NEString to copy from.
 * @param confidence An optional confidence level from 0.0 (min) to 1.0 (max).
 */
- (instancetype)initWithString:(NEString)value confidence:(double)confidence;

/**
 * @param value The primitive string value.
 */
- (instancetype)initWithRawValue:(NSString*)value;

/**
 * @param value The primitive string value.
 * @param confidence An optional confidence level from 0.0 (min) to 1.0 (max).
 */
- (instancetype)initWithRawValue:(NSString*)value confidence:(double)confidence;

/**
 * Parse a string in serialized form.
 * @param string The serialized string, as generated by serialize()
 *
 * @return `nil` if unserialization failed.
 */
+ (instancetype _Nullable)unserializeWithString:(NSString* _Nonnull)string;

@property(nonatomic, readonly) NEString value DEPRECATED_ATTRIBUTE;

/**
 * Underlying value.
 */
@property(nonatomic, readwrite) NSString* rawValue;

/**
 * @return A hash value representing the state of the Type.
 *          Can be used for checking equality.
 */
@property(nonatomic, readonly) NSUInteger hash;

/**
 * @return The name of the concrete value class extending NEXSensorItem.
 */
@property(nonatomic, readonly) NSString* type;

/**
 * @return A human-readable format of the held data.
 */
@property(nonatomic, readonly) NSString* description;

/**
 * @return A machine-readable format of the held data.  Returns `nil` if @p type is invalid, or if
 * serialization failed.
 */
- (NSString* _Nullable)serialize;

/**
 * Strict equality comparison.
 *
 * @see NEString_isEqual
 *
 * @param object Value to compare with.
 */
- (BOOL)isEqual:(id _Nullable)object;

/**
 * Convert the Objective-C class to a C-compatible struct.
 */
- (NEString)c_val;

@end
NS_ASSUME_NONNULL_END
