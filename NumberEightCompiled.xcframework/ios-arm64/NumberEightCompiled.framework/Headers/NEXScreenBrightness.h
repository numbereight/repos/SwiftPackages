//
//  NEXScreenBrightness.h
//  NumberEightCompiled
//
//  Created by Oliver Kocsis on 28/08/2018.
//  Copyright © 2018 NumberEight Technologies Ltd. All rights reserved.
//
#pragma once

#import "NEDouble.h"
#import "NEXSensorItem.h"

NS_ASSUME_NONNULL_BEGIN
@interface NEXScreenBrightness : NEXSensorItem

- (instancetype)init NS_UNAVAILABLE;
+ (instancetype)new NS_UNAVAILABLE;

- (instancetype)initWithBrightness:(NEDouble)brightness confidence:(double)confidence;

@property(nonatomic, readonly) double value;

@end
NS_ASSUME_NONNULL_END
