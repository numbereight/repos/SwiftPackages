//
//  NEXWebBridge.h
//  NumberEightCompiled
//
//  Created by Matthew Paletta on 2022-04-01.
//  Copyright © 2022 NumberEight Technologies Ltd. All rights reserved.
//
#pragma once

#import <Foundation/Foundation.h>

#import "NEXHTTPRequest.h"
#import "NEXHTTPResponse.h"

@interface NEXHTTPService : NSObject

+ (void)httpRequest:(NEXHTTPRequest* _Nonnull)request
           callback:(void (^_Nonnull)(NEXHTTPResponse* _Nonnull response))callback;

@end
