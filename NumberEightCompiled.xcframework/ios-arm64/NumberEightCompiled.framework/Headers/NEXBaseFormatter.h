//
//  BaseFormatter.h
//  NumberEightCompiled
//
//  Created by Matthew Paletta on 2021-05-14.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//
#pragma once
#import <Foundation/Foundation.h>

@class NEXSnapshot;

@interface NEXBaseFormatter : NSFormatter

///
/// Returns a new instance of a `NEXBaseFormatter`
///
- (instancetype _Nonnull)init;

///
/// Base implementation of turning a snapshot into a string.  Implementers should override this
/// method.
///
/// This base implementation returns nil for every input.
///
/// @param snapshot The snapshot to turn to a string
///
/// @return `nil` for all snapshots
///
- (NSString* _Nullable)stringFromSnapshot:(NEXSnapshot* _Nonnull)snapshot;

///
/// Overrides base implementation, calls `stringFromSnapshot`.
///
- (nullable NSString*)stringForObjectValue:(nullable id)obj;

@end
