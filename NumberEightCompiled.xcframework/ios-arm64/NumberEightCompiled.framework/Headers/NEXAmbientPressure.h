//
//  NEXAmbientPressure.h
//  NumberEightCompiled
//
//  Created by Oliver Kocsis on 28/08/2018.
//  Copyright © 2018 NumberEight Technologies Ltd. All rights reserved.
//
#pragma once

#import "NEDouble.h"
#import "NEXSensorItem.h"

NS_ASSUME_NONNULL_BEGIN
@interface NEXAmbientPressure : NEXSensorItem

- (instancetype)init NS_UNAVAILABLE;
+ (instancetype)new NS_UNAVAILABLE;

- (instancetype)initWithPressureInHPa:(NEDouble)pressureInHPa confidence:(double)confidence;

@property(nonatomic, readonly) double pressureInHPa;
@property(nonatomic, readonly) double pressureInKPa;

@end
NS_ASSUME_NONNULL_END
