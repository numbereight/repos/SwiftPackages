/**
 * @file NEBoolean.h
 * NEBoolean type.
 */

#ifndef NEBoolean_H
#define NEBoolean_H

#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Encapsulates a boolean
 */
typedef struct NEBoolean {
    bool value;
} NEBoolean;

/**
 * Default NEBoolean instance.
 */
static const NEBoolean NEBoolean_default = { .value = false };

/**
 * Returns true if the all the fields are identical in the two objects, false otherwise.
 *
 * @param lhsPtr A pointer to an NEBoolean struct to compare context in it.
 * @param rhsPtr A pointer to an NEBoolean struct to compare against.
 */
bool NEBoolean_isEqual(const NEBoolean* const lhsPtr, const NEBoolean* const rhsPtr);

#ifdef __cplusplus
}
#endif

#endif /* NEBoolean_H */
