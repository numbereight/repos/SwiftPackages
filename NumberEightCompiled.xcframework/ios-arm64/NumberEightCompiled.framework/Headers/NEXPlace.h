//
//  NEXPlace.h
//  NumberEightCompiled
//
//  Created by Oliver Kocsis on 19/06/2018.
//  Copyright © 2018 NumberEight Technologies Ltd. All rights reserved.
//
#pragma once

#import "NEPlace.h"
#import "NEXSensorItem.h"

NS_ASSUME_NONNULL_BEGIN

/**
 * Represents abstract information about a place, including a semantic name, major, and minor type.
 *
 * The name is a semantic name relevant to the user for the place: currently either home or work.
 * The major type represents a high-level category for the type of place.
 * The minor type gives a more granular category representation of the place.
 */
@interface NEXPlace : NEXSensorItem

/**
 * Default constructor.
 */
- (instancetype)init;

/**
 * Default constructor.
 */
+ (instancetype)new;

/**
 * Conversion copy constructor.
 * @param place NEPlace to copy from.
 */
- (instancetype)initWithPlace:(NEPlace)place;

/**
 * Conversion copy constructor.
 * @param place NEPlace to copy from.
 * @param confidence An optional confidence level from 0.0 (min) to 1.0 (max).
 */
- (instancetype)initWithPlace:(NEPlace)place confidence:(double)confidence;

/**
 * @param context Contextual name of the current place (e.g. NEPlaceContextHome).
 * @param major The major category type of the place (e.g. NEPlaceMajorFoodAndDrink).
 * @param minor The minor category type of the place (e.g. NEPlaceMinorBar).
 * @param brandID The ID for the brand of the place (e.g. 1 -> McDonald's)
 */
- (instancetype)initWithContext:(NEPlaceContext)context
                          major:(NEPlaceMajor)major
                          minor:(NEPlaceMinor)minor
                        brandID:(int)brandID;

/**
 * @param context Contextual name of the current place (e.g. NEPlaceContextHome).
 * @param major The major category type of the place (e.g. NEPlaceMajorFoodAndDrink).
 * @param minor The minor category type of the place (e.g. NEPlaceMinorBar).
 * @param brandID The ID for the brand of the place (e.g. 1 -> McDonald's)
 * @param confidence An optional confidence level from 0.0 (min) to 1.0 (max).
 */
- (instancetype)initWithContext:(NEPlaceContext)context
                          major:(NEPlaceMajor)major
                          minor:(NEPlaceMinor)minor
                        brandID:(int)brandID
                     confidence:(double)confidence;

/**
 * @param context Contextual name of the current place (e.g. NEPlaceContextHome).
 * @param major The major category type of the place (e.g. NEPlaceMajorFoodAndDrink).
 * @param minor The minor category type of the place (e.g. NEPlaceMinorBar).
 */
- (instancetype)initWithContext:(NEPlaceContext)context
                          major:(NEPlaceMajor)major
                          minor:(NEPlaceMinor)minor;

/**
 * @param context Contextual name of the current place (e.g. NEPlaceContextHome).
 * @param major The major category type of the place (e.g. NEPlaceMajorFoodAndDrink).
 * @param minor The minor category type of the place (e.g. NEPlaceMinorBar).
 * @param confidence An optional confidence level from 0.0 (min) to 1.0 (max).
 */
- (instancetype)initWithContext:(NEPlaceContext)context
                          major:(NEPlaceMajor)major
                          minor:(NEPlaceMinor)minor
                     confidence:(double)confidence;

/**
 * @param major The major category type of the place (e.g. NEPlaceMajorFoodAndDrink).
 * @param minor The minor category type of the place (e.g. NEPlaceMinorBar).
 * @param brandID The ID for the brand of the place (e.g. 1 -> McDonald's)
 */
- (instancetype)initWithMajor:(NEPlaceMajor)major minor:(NEPlaceMinor)minor brandID:(int)brandID;

/**
 * @param major The major category type of the place (e.g. NEPlaceMajorFoodAndDrink).
 * @param minor The minor category type of the place (e.g. NEPlaceMinorBar).
 * @param brandID The ID for the brand of the place (e.g. 1 -> McDonald's)
 * @param confidence An optional confidence level from 0.0 (min) to 1.0 (max).
 */
- (instancetype)initWithMajor:(NEPlaceMajor)major
                        minor:(NEPlaceMinor)minor
                      brandID:(int)brandID
                   confidence:(double)confidence;

/**
 * @param major The major category type of the place (e.g. NEPlaceMajorFoodAndDrink).
 * @param minor The minor category type of the place (e.g. NEPlaceMinorBar).
 */
- (instancetype)initWithMajor:(NEPlaceMajor)major minor:(NEPlaceMinor)minor;

/**
 * @param major The major category type of the place (e.g. NEPlaceMajorFoodAndDrink).
 * @param minor The minor category type of the place (e.g. NEPlaceMinorBar).
 * @param confidence An optional confidence level from 0.0 (min) to 1.0 (max).
 */
- (instancetype)initWithMajor:(NEPlaceMajor)major
                        minor:(NEPlaceMinor)minor
                   confidence:(double)confidence;

/**
 * @param major The major category type of the place (e.g. NEPlaceMajorFoodAndDrink).
 */
- (instancetype)initWithMajor:(NEPlaceMajor)major;

/**
 * @param major The major category type of the place (e.g. NEPlaceMajorFoodAndDrink).
 * @param confidence An optional confidence level from 0.0 (min) to 1.0 (max).
 */
- (instancetype)initWithMajor:(NEPlaceMajor)major confidence:(double)confidence;

/**
 * @param context Contextual name of the current place (e.g. NEPlaceContextHome).
 */
- (instancetype)initWithContext:(NEPlaceContext)context;

/**
 * @param context Contextual name of the current place (e.g. NEPlaceContextHome).
 * @param confidence An optional confidence level from 0.0 (min) to 1.0 (max).
 */
- (instancetype)initWithContext:(NEPlaceContext)context confidence:(double)confidence;

/**
 * Parse a string in serialized form.
 * @param string The serialized string, as generated by serialize()
 *
 * @return `nil` if unserialization failed.
 */
+ (instancetype _Nullable)unserializeWithString:(NSString*)string;

/**
 * @return A default NEPlace instance.
 */
+ (instancetype)defaultPlace;

/**
 * @return An NEPlace instance representing the case when the place cannot be determined.
 */
+ (instancetype)unavailable;

/**
 * @return An NEPlace instance representing the case when the user is not at a specific place.
 */
+ (instancetype)notAPlace;

/**
 * An NEPlace instance representing the case when the place type is unknown.
 */
+ (instancetype)unknown;

+ (instancetype)withContext:(NEPlaceContext)context;
+ (instancetype)withDefaultContextKnowledge:(NEPlaceContextKnowledge)knowledge;
+ (instancetype)withMajor:(NEPlaceMajor)major;
+ (instancetype)withMajorAndMinor:(NEPlaceMajor)major minor:(NEPlaceMinor)minor;
+ (instancetype)withMajorAndMinorAndBrand:(NEPlaceMajor)major
                                    minor:(NEPlaceMinor)minor
                                  brandID:(int)brandID;

@property(nonatomic, readonly) NEPlace value DEPRECATED_ATTRIBUTE;

/**
 * @return `true` if the Place major is not unavailable, notAPlace, or unknown.
 */
- (BOOL)isAPlace;

- (void)setContextKnowledgeAtIndex:(NEPlaceContextIndex)index
                         knowledge:(NEPlaceContextKnowledge)newKnowledge;
- (NEPlaceContextKnowledge)contextKnowledgeAtIndex:(NEPlaceContextIndex)index;
- (void)contextForEach:(void (^_Nullable)(NEPlaceContextIndex, NEPlaceContextKnowledge))cb;

+ (NSString*)stringFromContextIndex:(NEPlaceContextIndex)contextIndex;
+ (NSString*)stringFromContextKnowledge:(NEPlaceContextKnowledge)contextKnowledge;
+ (NSString*)stringFromMajor:(NEPlaceMajor)major;
+ (NSString*)stringFromMinor:(NEPlaceMinor)minor;

/**
 * High-level category for the place.
 */
@property(nonatomic, readwrite) NEPlaceMajor major;

/**
 * Granular category for the place.
 */
@property(nonatomic, readwrite) NEPlaceMinor minor;

/**
 * ID for the brand of the place.
 */
@property(nonatomic, readwrite) int brandID;

/**
 * @return A hash value representing the state of the Type.
 *          Can be used for checking equality.
 */
@property(nonatomic, readonly) NSUInteger hash;

/**
 * @return The name of the concrete value class extending NEXSensorItem.
 */
@property(nonatomic, readonly) NSString* type;

/**
 * @return A human-readable format of the held data.
 */
@property(nonatomic, readonly) NSString* description;

/**
 * @return A machine-readable format of the held data.  Returns `nil` if @p type is invalid, or if
 * serialization failed.
 */
- (NSString* _Nullable)serialize;

/**
 * Strict equality comparison.
 *
 * @see NEPlace_isEqual
 *
 * @param object Value to compare with.
 */
- (BOOL)isEqual:(id _Nullable)object;

/**
 * Convert the Objective-C class to a C-compatible struct.
 */
- (NEPlace)c_val;

@end
NS_ASSUME_NONNULL_END
