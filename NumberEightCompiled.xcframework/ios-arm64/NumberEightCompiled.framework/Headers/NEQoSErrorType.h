#ifndef NEQoSErrorType_H
#define NEQoSErrorType_H

// Use one on iOS, use the other within the core.
#if __has_include("NETypeUtils.h")
#include "NETypeUtils.h"
#else
#include "../../types/ctypes/NETypeUtils.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

/*
 * The list of known QoS Error types.  The error type should be set correctly when adding new error
 * types, or use `UnknownError`
 */
NE_ENUM(uint32_t, NEQoSErrorType){ NEQoSErrorTypeUnknownError = 0, NEQoSErrorTypeInvalidRequirement,
                                   NEQoSErrorTypeNotMeetingRequirement };

#ifdef __cplusplus
}
#endif

#endif
