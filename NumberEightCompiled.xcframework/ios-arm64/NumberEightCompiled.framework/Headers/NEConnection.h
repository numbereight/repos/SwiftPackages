/**
 * @file NEConnection.h
 * NEConnection type.
 */

#ifndef NEConnection_H
#define NEConnection_H

#include <stdbool.h>

#include "NETypeUtils.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Possible states for NEConnection.
 */
NE_ENUM(uint32_t, NEConnectionState){ NEConnectionStateUnknown = 0, NEConnectionStateDisconnected,
                                      NEConnectionStateConnected };

/**
 * Represents whether the device is moving or not moving.
 */
typedef struct NEConnection {
    /**
     * Whether the device is moving or not moving.
     */
    NEConnectionState state;
} NEConnection;

/**
 * Default NEConnection instance.
 */
static const NEConnection NEConnection_default = { .state = NEConnectionStateUnknown };

/**
 * C String array mapping for NEConnectionState
 */
static const char* const NEConnectionStateStrings[] = {
    [NEConnectionStateUnknown] = "Unknown",
    [NEConnectionStateDisconnected] = "Disconnected",
    [NEConnectionStateConnected] = "Connected",
};

static const char* const NEConnectionStateReprs[] = {
    [NEConnectionStateUnknown] = "unknown",
    [NEConnectionStateDisconnected] = "disconnected",
    [NEConnectionStateConnected] = "connected",
};

const char* NEConnection_stringFromState(NEConnectionState state);
const char* NEConnection_reprFromState(NEConnectionState state);
NEConnectionState NEConnection_stateFromRepr(const char* repr);

/**
 * Returns true if the all the fields are identical in the two objects, false otherwise.
 *
 * @param lhsPtr A pointer to an NEConnection struct to compare context in it.
 * @param rhsPtr A pointer to an NEConnection struct to compare against.
 */
bool NEConnection_isEqual(const NEConnection* const lhsPtr, const NEConnection* const rhsPtr);

#ifdef __cplusplus
}
#endif

#endif /* NEConnection_H */
