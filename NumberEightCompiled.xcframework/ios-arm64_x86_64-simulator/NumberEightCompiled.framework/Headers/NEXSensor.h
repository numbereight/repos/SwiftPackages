//
//  NEXSensor.h
//  NumberEightCompiled
//
//  Created by Oliver Kocsis on 04/09/2018.
//  Copyright © 2018 NumberEight Technologies Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Consentable.h"
#import "NEXConsentOptionsPropertyGroup.h"
#import "NEXGlimpse.h"
#import "NEXQoSLevel.h"

@class NEXEngine;

typedef void (^NEXSensorRequestCallback)(NEXGlimpse* _Nonnull glimpse);

/**
 * A Sensor is an abstraction that provides an inheritable interface
 * suitable for publishing data to an `NEXEngine`.
 */
@interface NEXSensor : NSObject

/**
 * @return The topic name the sensor publishes to.  Returns nil if the sensor is not initialized.
 */
@property(nonatomic, readonly, nullable) NSString* topic;

/**
 * @return The numerical ID of the sensor.  Returns 0 if not initialized.
 */
@property(nonatomic, readonly) NSInteger ID;

/**
 * @return The full path (topic/id) that the sensor is registered on.  Returns nil if the sensor is
 * not initialized.
 */
@property(nonatomic, readonly, nullable) NSString* path;

/**
 * @return Returns whether the sensor has started.
 */
@property(nonatomic, readonly) bool isActive;

/**
 * @return Returns whether `[NEXSensor initSensorWithError]` has been called.
 */
@property(nonatomic, readonly) bool isInitialized;

/**
 * @return The required sensor for the sensor to run as specified in the constructor of the sensor.
 */
@property(nonatomic, readonly) NEXConsentOptionsPropertyGroup* _Nonnull requiredConsent;

/**
 * Creates a new sensor instance.
 */
- (instancetype _Nonnull)initWithEngine:(NEXEngine* _Nonnull)engine
                                  topic:(NSString* _Nonnull)topic
                        requiredConsent:(NEXConsentOptionsPropertyGroup* _Nonnull)requiredConsent;

- (instancetype _Nonnull)init NS_UNAVAILABLE;
+ (instancetype _Nonnull)new NS_UNAVAILABLE;

/**
 * Requests a new Event from the sensor.
 *
 * @see `[NEXEngine request]`
 * @param callback Called when request is complete.
 * @param error The error if one occured while requesting data
 * @return Boolean indiciating if the request was successfully made or not.
 * `callback` is guaranteed to be called either once or not at all.
 */
- (BOOL)request:(NEXSensorRequestCallback _Nonnull)callback
          error:(__autoreleasing NSError* _Nullable* _Nullable)error;

/**
 * Initialises the sensor, registering it with the `NEXEngine`.
 * Must be called before using the sensor. It is allowable to call `[NEXSensor initSensorWithError]`
 * from the constructor of a concrete class that extends `NEXSensor`.
 */
- (BOOL)initSensorWithError:(__autoreleasing NSError* _Nullable* _Nullable)error;

/**
 * Permanently stops the sensor, disabling all future publish calls
 * and event handler invocations. `[NEXSensor onStop]` will be called first.
 */
- (BOOL)closeWithError:(__autoreleasing NSError* _Nullable* _Nullable)error;

@end

@interface NEXSensor (Internal)

/**
 * Publishes an `NEXGlimpse` to the `Engine`.
 *
 * @param glimpse Glimpse to publish.
 * @throws An error if the channel is already permanently closed.
 */
- (BOOL)publishGlimpse:(NEXGlimpse* _Nonnull)glimpse
                 error:(__autoreleasing NSError* _Nullable* _Nullable)error NS_REFINED_FOR_SWIFT;

/**
 * Performs @p task asynchronously every @p interval while the sensor is active.
 * Useful for publishing events regularly.
 * The first call to @p task will happen immediately if the sensor is started,
 * or as soon as the sensor is started otherwise.
 * Must be called after `[NEXSensor initWithError]` has finished.
 *
 * @param initialInterval The interval between each call to @p task.
 * @param handler The function to call.
 * @return A task ID that can be used with `[NEXSensor cancelTaskWithId]`  to stop any further
 * calls.
 */
- (NSUUID* _Nullable)everyWithInitialInterval:(NSTimeInterval)initialInterval
                                      handler:(void (^_Nonnull)(void))handler
                                        error:(__autoreleasing NSError* _Nullable* _Nullable)error;

/**
 * Performs @p task asynchronously every @p recurringInterval while the sensor is active.
 * Useful for publishing events regularly.
 * The first call  to @p task will only happen after @p initialInterval.
 * Must be called after `[NEXSensor initWithError]` has finished.
 *
 * @param initialInterval The interval between the first call to @p task.
 * @param recurringInterval The interval between each subsequent call to @p task.
 * @param handler The function to call.
 * @return A task ID that can be used with `[NEXSensor cancelTaskWithId]` to stop any further calls.
 */
- (NSUUID* _Nullable)everyWithInitialInterval:(NSTimeInterval)initialInterval
                                    recurring:(NSTimeInterval)recurringInterval
                                      handler:(void (^_Nonnull)(void))handler
                                        error:(__autoreleasing NSError* _Nullable* _Nullable)error;

/**
 * Cancels a task created by `[NEXSensor everyWithInitialInterval]`.
 * @param task The ID of the task, as returned by `[NEXSensor everyWithInitialInterval]`.
 */
- (BOOL)cancelTaskWithId:(NSUUID* _Nonnull)task
                   error:(__autoreleasing NSError* _Nullable* _Nullable)error;

/**
 * Called once each time the sensor starts or restarts.
 * Defaults to do nothing.
 */
- (void)onStart;

/**
 * Called once each time the sensor stops.
 * Defaults to do nothing.
 */
- (void)onStop;

/**
 * Called when a request is made from the `[NEXEngine]` to publish
 * an event immediately.
 * Defaults to retrieving the last known event from the `[NEXEngine]`.
 *
 * @param callback The callback to invoke once a new event is ready.
 */
- (void)onRequest:(NEXSensorRequestCallback _Nonnull)callback;

/**
 * Called when the Sensor QoS Level is updated by the Engine. The sensor should
 * propagate this change to its own subscriptions, if appropriate.
 *
 * @param qos The QoS level for this sensor, as requested by the subscriber.
 *
 * @return The optional error if the sensor was unable to update to its new requested QoS Level,
 * `NEXQoSLevel`. This error will be propagated to `[NEXSubscription notifyQoSViolation]` for all
 * subscribers (future).
 *
 * @see `NEXQoSError`
 */
- (NEXQoSError* _Nullable)onSetQoS:(NEXQoSLevel* _Nonnull)qos;

@end

@interface NEXSensor () <Consentable>
@end
