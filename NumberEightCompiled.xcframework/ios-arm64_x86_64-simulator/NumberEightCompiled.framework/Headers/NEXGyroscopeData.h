//
//  NEXGyroscopeData.h
//  NumberEightCompiled
//
//  Created by Oliver Kocsis on 28/08/2018.
//  Copyright © 2018 NumberEight Technologies Ltd. All rights reserved.
//
#pragma once

#import <CoreMotion/CMGyro.h>

#import "NEXVector3D.h"

NS_ASSUME_NONNULL_BEGIN
@interface NEXGyroscopeData : NEXVector3D

- (instancetype)init NS_UNAVAILABLE;
+ (instancetype)new NS_UNAVAILABLE;

- (instancetype)initWithVector:(NEVector3D)rotationRate confidence:(double)confidence;

@property(nonatomic, readonly) CMRotationRate cmRotationRate
    __attribute__((deprecated("use rotation instead.")));
@property(nonatomic, readonly) NEVector3D rotation;

@end
NS_ASSUME_NONNULL_END
