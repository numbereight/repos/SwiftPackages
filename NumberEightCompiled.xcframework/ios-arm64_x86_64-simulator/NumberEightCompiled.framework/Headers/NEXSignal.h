//
//  NEXLocationCluster.h
//  NumberEightCompiled
//
//  Created by Oliver Kocsis on 19/06/2018.
//  Copyright © 2018 NumberEight Technologies Ltd. All rights reserved.
//
#pragma once

#import "NESignal.h"
#import "NEXSensorItem.h"

NS_ASSUME_NONNULL_BEGIN

/**
 * Represents a signal strength to a connected radio base station.
 *
 * This could be Wifi, cellular, Bluetooth, and more.
 * It contains an identifier for the base station, local and remote addresses,
 * and a signal strength in dBm.
 *
 * For cellular towers, the baseStationID is a concatenation of MCC, MNC, LAC or TAC, and CID or CI.
 * For Wifi, this is the BSSID.
 */
@interface NEXSignal : NEXSensorItem

/**
 * Default constructor.
 */
- (instancetype)init;

/**
 * Default constructor.
 */
+ (instancetype)new;

/**
 * Conversion copy constructor.
 * @param signal NESignal to copy from.
 */
- (instancetype)initWithSignal:(NESignal)signal;

/**
 * Conversion copy constructor.
 * @param signal NESignal to copy from.
 * @param confidence An optional confidence level from 0.0 (min) to 1.0 (max).
 */
- (instancetype)initWithSignal:(NESignal)signal confidence:(double)confidence;

/**
 * @param baseStationID Name or ID of the relevant base station.
 * @param strength Strength of the signal in decibels.
 */
- (instancetype)initWithBaseStationID:(NSString*)baseStationID strength:(int)strength;

/**
 * @param baseStationID Name or ID of the relevant base station.
 * @param strength Strength of the signal in decibels.
 * @param confidence An optional confidence level from 0.0 (min) to 1.0 (max).
 */
- (instancetype)initWithBaseStationID:(NSString*)baseStationID
                             strength:(int)strength
                           confidence:(double)confidence;

/**
 * @param baseStationID Name or ID of the relevant base station.
 * @param strength Strength of the signal in decibel milliwatts.
 * @param localAddress A network address of the device locally to the base station.
 * @param gatewayAddress A network address of the base station.
 * @param remoteAddress A remote network address of the device via the base station.
 */
- (instancetype)initWithBaseStationID:(NSString*)baseStationID
                             strength:(int)strength
                         localAddress:(NSString*)localAddress
                       gatewayAddress:(NSString*)gatewayAddress
                        remoteAddress:(NSString*)remoteAddress;

- (instancetype)initWithBaseStationID:(NSString*)baseStationID
                             strength:(int)strength
                         localAddress:(NSString*)localAddress
                       gatewayAddress:(NSString*)gatewayAddress
                        remoteAddress:(NSString*)remoteAddress
                           confidence:(double)confidence;

/**
 * Parse a string in serialized form.
 * @param string The serialized string, as generated by serialize()
 *
 * @return `nil` if unserialization failed.
 */
+ (instancetype _Nullable)unserializeWithString:(NSString* _Nonnull)string;

@property(nonatomic, readonly, class) NEXSignal* defaultSignal;

@property(nonatomic, readonly) NESignal value DEPRECATED_ATTRIBUTE;

/**
 * A unique identifier for the base station.
 * For cellular towers, the baseStationID is a concatenation of MCC, MNC, LAC or TAC, and CID or CI.
 * For Wifi, this is the SSID or BSSID.
 *
 * This will be the empty string if not found.
 */
@property(nonatomic, readonly, nonnull) NSString* baseStationID;

/**
 * Signal strength in dBm.
 */
@property(nonatomic, readonly) NSInteger strength;

/**
 * A local network address for the device connected to the base station.
 * This may be a MAC address, IPv4, or IPv6 address.
 *
 * This will be the empty string if not found.
 */
@property(nonatomic, readonly, nonnull) NSString* localAddress;

/**
 * A local network address for the base station.
 * This may be a MAC address, IPv4, or IPv6 address.
 *
 * This will be the empty string if not found.
 */
@property(nonatomic, readonly, nonnull) NSString* gatewayAddress;

/**
 * A remote network address for the device via this base station.
 * This may be a MAC address, IPv4, or IPv6 address.
 *
 * This will be the empty string if not found.
 */
@property(nonatomic, readonly, nonnull) NSString* remoteAddress;

/**
 * @return A hash value representing the state of the Type.
 *          Can be used for checking equality.
 */
@property(nonatomic, readonly) NSUInteger hash;

/**
 * @return The name of the concrete value class extending NEXSensorItem.
 */
@property(nonatomic, readonly) NSString* type;

/**
 * @return A human-readable format of the held data.
 */
@property(nonatomic, readonly) NSString* description;

/**
 * @return A machine-readable format of the held data.  Returns `nil` if @p type is invalid, or if
 * serialization failed.
 */
- (NSString* _Nullable)serialize;

/**
 * Strict equality comparison.
 *
 * @see NESignal_isEqual
 *
 * @param object Value to compare with.
 */
- (BOOL)isEqual:(id _Nullable)object;

/**
 * Convert the Objective-C class to a C-compatible struct.
 */
- (NESignal)c_val;

@end
NS_ASSUME_NONNULL_END
