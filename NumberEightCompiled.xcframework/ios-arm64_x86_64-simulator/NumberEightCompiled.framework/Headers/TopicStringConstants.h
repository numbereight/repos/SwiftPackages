/**
 * @file TopicStringConstants.h
 * Constants for common topic names (see source code).
 */
#ifndef TOPIC_STRING_CONSTANTS_H
#define TOPIC_STRING_CONSTANTS_H

#ifndef TopicStrType
// iOS (engine/NEXTopics.h defines NE_USE_IOS_TOPICS)
#ifdef NE_USE_IOS_TOPICS
#import <Foundation/Foundation.h>
// Turn strings into NSStrings, init with @("<string>")
#define TopicStrType NSString* const
#define TopicStrVal(val) @(val)
#else
// Leave strings as C strings
#define TopicStrType constexpr const char*
#define TopicStrVal(val) val
#endif
#endif  // end TopicStrType

static TopicStrType kNETopicInternal = TopicStrVal("_");

#pragma mark High Level Context
static TopicStrType kNETopicActivity = TopicStrVal("activity");
static TopicStrType kNETopicConnectionPerformance = TopicStrVal("network/connection_performance");
static TopicStrType kNETopicDeviceMovement = TopicStrVal("motion/device_movement");
static TopicStrType kNETopicDevicePosition = TopicStrVal("device_position");
static TopicStrType kNETopicIndoorOutdoor = TopicStrVal("indoor_outdoor");
static TopicStrType kNETopicJogging = TopicStrVal("extended/jogging");
static TopicStrType kNETopicLockStatus = TopicStrVal("lock_status");
static TopicStrType kNETopicPlace = TopicStrVal("place");
static TopicStrType kNETopicReachability = TopicStrVal("reachability");
static TopicStrType kNETopicAdjustedReachability = TopicStrVal("adjusted_reachability");
static TopicStrType kNETopicSituation = TopicStrVal("situation");
static TopicStrType kNETopicTime = TopicStrVal("time");
static TopicStrType kNETopicUserMovement = TopicStrVal("motion/user_movement");
static TopicStrType kNETopicWeather = TopicStrVal("weather");

#pragma mark Low Level Context
static TopicStrType kNETopicAcceleration = TopicStrVal("motion/acceleration");
static TopicStrType kNETopicMagneticVariance = TopicStrVal("magnetism/magnetic_variance");
static TopicStrType kNETopicNativeActivity = TopicStrVal("motion/native/activity");
static TopicStrType kNETopicNativeDeviceMotion = TopicStrVal("motion/native/device_motion");
static TopicStrType kNETopicNativeSignificantMotion = TopicStrVal("motion/native/significant");
static TopicStrType kNETopicNativeStepCounter = TopicStrVal("motion/native/step_counter");
static TopicStrType kNETopicNativeStepDetector = TopicStrVal("motion/native/step");
static TopicStrType kNETopicRelativeAltitude = TopicStrVal("ambient/relative_altitude");
static TopicStrType kNETopicSunlight = TopicStrVal("sunlight");
static TopicStrType kNETopicSpeed = TopicStrVal("motion/speed");
static TopicStrType kNETopicGPSSpeed = TopicStrVal("motion/speed_gps");
static TopicStrType kNETopicLowPowerSpeed = TopicStrVal("motion/speed_low_power");

#pragma mark Sensor Topics
static TopicStrType kNETopicAccelerometer = TopicStrVal("motion/accelerometer");
static TopicStrType kNETopicAccelerometerStatus = TopicStrVal("status/motion/accelerometer");
static TopicStrType kNETopicAmbientTemperature = TopicStrVal("ambient/temperature");
static TopicStrType kNETopicAmbientPressure = TopicStrVal("ambient/pressure");
static TopicStrType kNETopicAmbientHumidity = TopicStrVal("ambient/humidity");
static TopicStrType kNETopicAmbientLight = TopicStrVal("ambient/light");
static TopicStrType kNETopicAmbientLightDetected = TopicStrVal("ambient/light/detected");
static TopicStrType kNETopicAmbientLightInferred = TopicStrVal("ambient/light/inferred");
static TopicStrType kNETopicBatteryTemperature = TopicStrVal("battery/temperature");
static TopicStrType kNETopicBatteryLevel = TopicStrVal("battery/level");
static TopicStrType kNETopicCalibratedMagnetometer =
    TopicStrVal("magnetism/calibrated_magnetometer");
static TopicStrType kNETopicGyroscope = TopicStrVal("motion/gyroscope");
static TopicStrType kNETopicGeo = TopicStrVal("geo");
static TopicStrType kNETopicLatency = TopicStrVal("network/latency");
static TopicStrType kNETopicLatencyUpload = TopicStrVal("network/latency_upload");
static TopicStrType kNETopicMagnetometer = TopicStrVal("magnetism/magnetometer");
static TopicStrType kNETopicProximity = TopicStrVal("proximity");
static TopicStrType kNETopicScreenBrightness = TopicStrVal("screen_brightness");
static TopicStrType kNETopicVolumeLevelRaw = TopicStrVal("volume/level/raw");
static TopicStrType kNETopicVolumeLevelCategory = TopicStrVal("volume/level/category");
static TopicStrType kNETopicVolumeLevelGranularCategory =
    TopicStrVal("volume/level/granular/category");

#pragma mark Radio Topics
static TopicStrType kNETopicBluetoothClassicScanner =
    TopicStrVal("signal/bluetooth/classic/scanner");
static TopicStrType kNETopicBluetoothLEScanner = TopicStrVal("signal/bluetooth/le/scanner");
static TopicStrType kNETopicCellularConnection = TopicStrVal("signal/cellular/connection");
static TopicStrType kNETopicWifiConnection = TopicStrVal("signal/wifi/connection");
static TopicStrType kNETopicWifiScanner = TopicStrVal("signal/wifi/scanner");

#pragma mark Peripherals Topics
static TopicStrType kNETopicHeadphones = TopicStrVal("devices/audio/headphones");
static TopicStrType kNETopicHeadphonesWired = TopicStrVal("devices/audio/headphones/wired");
static TopicStrType kNETopicHeadphonesBluetooth = TopicStrVal("devices/audio/headphones/bluetooth");

#pragma mark Place Helper Topics
static TopicStrType kNETopicNaivePlace = TopicStrVal("_/place_naive");
static TopicStrType kNETopicPlaceContextInternal = TopicStrVal("_/place_context");
static TopicStrType kNETopicPlaceInternal = TopicStrVal("_/place");

#pragma mark Location Topics
static TopicStrType kNETopicCLRegions = TopicStrVal("location/regions");
static TopicStrType kNETopicCLBackgroundLocation = TopicStrVal("location/background_location");
static TopicStrType kNETopicGPSLocation = TopicStrVal("location/gps");
static TopicStrType kNETopicLazyGPS = TopicStrVal("gps/lazy");
static TopicStrType kNETopicLocation = TopicStrVal("location");
static TopicStrType kNETopicLocationCluster = TopicStrVal("location_cluster");
static TopicStrType kNETopicLowPowerLocation = TopicStrVal("location/low_power");
static TopicStrType kNETopicLowPowerIPLocation = TopicStrVal("location/low_power/ip");
static TopicStrType kNETopicLSTMCluster = TopicStrVal("location_cluster_lstm");

#ifndef NE_USE_IOS_TOPICS
// Not available on iOS
#include <string>
#define NTH_TOPIC(topic, nth) std::string(topic) + "/" + std::to_string(nth)
#endif

#endif  // File Guard end
