#ifndef NEHTTPMethod_H
#define NEHTTPMethod_H

// Use one on iOS, use the other within the core.
#if __has_include("NETypeUtils.h")
#include "NETypeUtils.h"
#else
#include "../types/ctypes/NETypeUtils.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

NE_ENUM(uint32_t, NEHTTPMethod){ GET, HEAD, POST, DELETE, PUT, CONNECT, OPTIONS, TRACE, PATCH };

#ifdef __cplusplus
}
#endif

#endif /* NEHTTPMethod_H */
