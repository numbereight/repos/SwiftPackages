//
//  NEXQueryStringFormatter.h
//  NumberEightCompiled
//
//  Created by Matthew Paletta on 2021-05-17.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

#import "NEXBaseFormatter.h"

#import <Foundation/Foundation.h>

///
/// Implementation to serialize a `NEXSnapshot` as a query string.
///
@interface NEXQueryStringFormatter : NEXBaseFormatter

///
/// Returns a new instance of a `NEXQueryStringFormatter`
///
-(instancetype _Nonnull)init;

///
/// Overrides `NEXBaseFormatter` to represent a snapshot as a serialized query string.
///
/// @note This is thread safe
/// @return Query string representation of the snapshot
///
/// @code
/// NEXSnapshotter* snapshotter = [[NEXSnapshotter alloc] initWithDefaults];
///
/// NEXSnapshot* snapshot = [snapshotter takeSnapshot];
///
/// //...
///
/// NEXQueryStringFormatter* qsFormatter = [[NEXQueryStringFormatter alloc] init];
/// NSString* qsString = [qsFormatter stringFromSnapshot:snapshot];
///
/// qsString == @"time=early-morning,weekday&weather=cold,snow";
/// @endcode
///
-(NSString* _Nullable)stringFromSnapshot:(NEXSnapshot* _Nonnull)snapshot;

@end
