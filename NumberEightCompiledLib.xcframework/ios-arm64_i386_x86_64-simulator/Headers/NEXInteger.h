//
//  NEXInteger.h
//  NumberEightCompiled
//
//  Created by Matthew Paletta on 2021-05-20.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//


#import "NEXSensorItem.h"
#include "NEInteger.h"

NS_ASSUME_NONNULL_BEGIN

@interface NEXInteger : NEXSensorItem

-(instancetype)init NS_UNAVAILABLE;
+(instancetype)new NS_UNAVAILABLE;

-(instancetype)initWithValue:(int)value
                  confidence:(double)confidence;

-(instancetype)initWithValue:(int)value;

@property (nonatomic, readonly) NEInteger value;

@end

NS_ASSUME_NONNULL_END
