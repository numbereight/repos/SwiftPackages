//
//  NEXBoolean.h
//  NumberEightCompiled
//
//  Created by Matthew Paletta on 2021-05-11.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//
#pragma once

#import "NEXSensorItem.h"
#include "NEBoolean.h"

/**
 * Contains a NEBoolean value which represents a boolean value.
 * Defaults to `false`.
 */
NS_ASSUME_NONNULL_BEGIN

@interface NEXBoolean : NEXSensorItem

-(instancetype)init NS_UNAVAILABLE;
+(instancetype)new NS_UNAVAILABLE;

@property (nonatomic, readonly) NEBoolean value;

@end

NS_ASSUME_NONNULL_END
