//
//  NEXLocationCluster.h
//  NumberEightCompiled
//
//  Created by Oliver Kocsis on 19/06/2018.
//  Copyright © 2018 NumberEight Technologies Ltd. All rights reserved.
//

#include "NESignal.h"

#import "NEXSensorItem.h"

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@interface NEXSignal : NEXSensorItem

-(instancetype)init NS_UNAVAILABLE;
+(instancetype)new NS_UNAVAILABLE;

@property (nonatomic, readonly) NESignal value DEPRECATED_ATTRIBUTE;

/**
 * A unique identifier for the base station.
 * For cellular towers, the baseStationID is a concatenation of MCC, MNC, LAC or TAC, and CID or CI.
 * For Wifi, this is the SSID or BSSID.
 *
 * This will be the empty string if not found.
 */
@property (nonatomic, readonly, nonnull) NSString* baseStationID;

/**
 * Signal strength in dBm.
 */
@property (nonatomic, readonly) NSInteger strength;

/**
 * A local network address for the device connected to the base station.
 * This may be a MAC address, IPv4, or IPv6 address.
 *
 * This will be the empty string if not found.
 */
@property (nonatomic, readonly, nonnull) NSString* localAddress;

/**
 * A local network address for the base station.
 * This may be a MAC address, IPv4, or IPv6 address.
 *
 * This will be the empty string if not found.
 */
@property (nonatomic, readonly, nonnull) NSString* gatewayAddress;

/**
 * A remote network address for the device via this base station.
 * This may be a MAC address, IPv4, or IPv6 address.
 *
 * This will be the empty string if not found.
 */
@property (nonatomic, readonly, nonnull) NSString* remoteAddress;

@end
NS_ASSUME_NONNULL_END
