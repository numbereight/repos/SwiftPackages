//
//  NEXEngine+internal.h
//  NumberEightCompiled
//
//  Created by Matt on 2021-08-06.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

#pragma once
#include "NEXEngine.h"

@interface NEXEngine(internal)

-(bool)isRunning;

@end
