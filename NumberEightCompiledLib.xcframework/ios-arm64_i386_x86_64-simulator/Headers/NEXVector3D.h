//
//  NEXVector3D.h
//  NumberEightCompiled
//
//  Created by Matthew Paletta on 2021-05-20.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

#import "NEXSensorItem.h"
#include "NEVector3D.h"

NS_ASSUME_NONNULL_BEGIN

@interface NEXVector3D : NEXSensorItem

-(instancetype)init NS_UNAVAILABLE;
+(instancetype)new NS_UNAVAILABLE;

@property (nonatomic, readonly) NEVector3D value;

@end

NS_ASSUME_NONNULL_END
