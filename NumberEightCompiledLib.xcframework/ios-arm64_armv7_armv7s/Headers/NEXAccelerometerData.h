//
//  NEXAccelerometerData.h
//  NumberEightCompiled
//
//  Created by Oliver Kocsis on 28/08/2018.
//  Copyright © 2018 NumberEight Technologies Ltd. All rights reserved.
//

#import "NEXVector3DData.h"
#import <CoreMotion/CMAccelerometer.h>

NS_ASSUME_NONNULL_BEGIN
@interface NEXAccelerometerData : NEXVector3DData

-(instancetype)init NS_UNAVAILABLE;
+(instancetype)new NS_UNAVAILABLE;

@property (nonatomic, readonly) CMAcceleration cmAcceleration __attribute__((deprecated("use acceleration instead.")));
@property (nonatomic, readonly) NEVector3D acceleration;

@end
NS_ASSUME_NONNULL_END
