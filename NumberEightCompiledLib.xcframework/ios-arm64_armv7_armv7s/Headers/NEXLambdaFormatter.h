//
//  LambdaFormatter.h
//  NumberEightCompiled
//
//  Created by Matthew Paletta on 2021-10-14.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

#import "NEXBaseFormatter.h"
#import "NEXLambdaFormatterTypes.h"

@interface NEXLambdaFormatter : NEXBaseFormatter

-(instancetype _Nonnull)init NS_UNAVAILABLE;
-(instancetype _Nonnull)new NS_UNAVAILABLE;

///
/// Returns a new instance of a `NEXLambdaFormatter`
///
-(instancetype _Nonnull)initWithBlock:(NEXSnapshotFormatterBlock _Nonnull )block;

///
/// Overrides `NEXBaseFormatter` to represent a snapshot as a serialized string, using the block.
///
/// @return Query string representation of the snapshot
///
-(NSString* _Nullable)stringFromSnapshot:(NEXSnapshot* _Nonnull)snapshot;

@end
