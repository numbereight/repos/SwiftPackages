//
//  NEXConsoleLogSink.h
//  NumberEightCompiled
//
//  Created by Matthew Paletta on 2021-06-07.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NEXEngine.h"

NS_ASSUME_NONNULL_BEGIN

@interface NEXConsoleLogSink : NSObject<NEXLogSinkDelegate>

@property (atomic, readwrite) NSUInteger minLogLevel;

-(instancetype)init;

-(void)onMessageLevel:(int)level tag:(NSString *)tag message:(NSString *)message;

@end

NS_ASSUME_NONNULL_END
