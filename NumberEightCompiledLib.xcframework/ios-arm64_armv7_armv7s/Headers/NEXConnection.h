//
//  NEXConnection.h
//  NumberEightCompiled
//
//  Created by Matthew Paletta on 2021-03-11.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "NEXSensorItem.h"
#import "NEConnection.h"

NS_ASSUME_NONNULL_BEGIN

@interface NEXConnection : NEXSensorItem

-(instancetype)init NS_UNAVAILABLE;
+(instancetype)new NS_UNAVAILABLE;

@property (nonatomic, readonly) NEConnection value;

+(NSString *)stringFromConnectionState:(NEConnectionState)state;

@end

NS_ASSUME_NONNULL_END
