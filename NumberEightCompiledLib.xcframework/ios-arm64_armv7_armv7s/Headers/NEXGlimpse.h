//
//  NEXGlimpse.h
//  NumberEightCompiled
//
//  Created by Oliver Kocsis on 18/10/2018.
//  Copyright © 2018 NumberEight Technologies Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NEXSensorItem.h"

NS_ASSUME_NONNULL_BEGIN

/**
 @interface NEXGlimpse
 A snapshot of a user's current context.
 
 A Glimpse contains a list of possibilities orderred by their confidence
 along with when it was first recorded.
 */
NS_SWIFT_NAME(Glimpse)
@interface NEXGlimpse<__covariant NEXSensorItemType> : NSObject

- (instancetype)initWithArray:(NSArray<NEXSensorItemType> *)possibilities
                 sensorUptime:(NSTimeInterval)sensorUptime
                  arrivalDate:(NSDate *)arrivalDate
                        topic:(NSString *)topic;


-(instancetype)init NS_UNAVAILABLE;
+(instancetype)new NS_UNAVAILABLE;

/**
 The most probable value from the list of possibilities.
 */
@property (nonatomic, readonly) NEXSensorItemType mostProbable;

/**
 The confidence of the most probable value from the list of possibilities.
 */
@property (nonatomic, readonly) double mostProbableConfidence;

/**
 A list of value and their confidence level from 0.0 to 1.0.
 The list is sorted from most to least probable.
 This is akin to a distribution over all possible states, and where
 there are multiple values, the probabilities add up to 1.0.
 
 N.B. NELocation will give its confidence as 1 / accuracy_in_metres
 */
@property (nonatomic, readonly) NSArray<NEXSensorItemType> *possibilities;

/**
 A time interval in fractional seconds of the sensor's reported uptime.
 If unavailable, the time will be -Inf.
 */
@property (nonatomic, readonly) NSTimeInterval sensorUptime;

/**
 The time at which the glimpse was first created.
 */
@property (nonatomic, readonly) NSDate *createdAt;

/**
 The original topic string that the glimpse was published to.
 */
@property (nonatomic, readonly) NSString *topic;

@property (class, readonly) double kNESensorUptimeNotAvailable;

@end

NS_ASSUME_NONNULL_END
