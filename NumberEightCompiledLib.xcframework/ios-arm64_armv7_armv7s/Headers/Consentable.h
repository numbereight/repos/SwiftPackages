//
//  Consentable.h
//  NumberEightCompiled
//
//  Created by Matthew Paletta on 2020-10-26.
//  Copyright © 2020 NumberEight Technologies Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol Consentable

@property (nonatomic, readonly) NSArray<NSNumber*>* requiredConsent;

@end
