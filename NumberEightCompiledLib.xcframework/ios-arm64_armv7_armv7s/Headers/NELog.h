//
//  NELog.h
//  NumberEightCompiled
//
//  Created by Matthew Paletta on 2021-04-07.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

#if DEBUG
#define NEX_TAG_NULLABLE _Nullable
#else
#define NEX_TAG_NULLABLE _Nonnull
#endif

NS_ASSUME_NONNULL_BEGIN

@interface NELog : NSObject

+(void)msg:(NSString* NEX_TAG_NULLABLE)tag error:(NSString* _Nullable)msg;
+(void)msg:(NSString* NEX_TAG_NULLABLE)tag warning:(NSString* _Nullable)msg;
+(void)msg:(NSString* NEX_TAG_NULLABLE)tag info:(NSString* _Nullable)msg;
+(void)msg:(NSString* NEX_TAG_NULLABLE)tag debug:(NSString* _Nullable)msg;
+(void)msg:(NSString* NEX_TAG_NULLABLE)tag verbose:(NSString* _Nullable)msg;

@end

NS_ASSUME_NONNULL_END

#undef NEX_TAG_NULLABLE
