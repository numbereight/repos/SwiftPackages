//
//  NEXDataProvider.h
//  NumberEightCompiled
//
//  Created by Matthew Paletta on 2021-04-07.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol DataProvider

/**
 Delegate callback executed when `NumberEight.deleteUserData` is called.  The listener should remove all user data from its own service.
 */
-(void)performDataRemoval;

@end

NS_ASSUME_NONNULL_END
