//
//  NEXLocationClusterID.h
//  NumberEightCompiled
//
//  Created by Oliver Kocsis on 28/08/2018.
//  Copyright © 2018 NumberEight Technologies Ltd. All rights reserved.
//

#import "NEXSensorItem.h"

NS_ASSUME_NONNULL_BEGIN
@interface NEXLocationClusterID : NEXSensorItem

@property (nonatomic, readonly) int32_t clusterID;

@end
NS_ASSUME_NONNULL_END
