//
//  NEXReachability.h
//  NumberEightCompiled
//
//  Created by Oliver Kocsis on 21/06/2018.
//  Copyright © 2018 NumberEight Technologies Ltd. All rights reserved.
//

#import "NEXSensorItem.h"

#import "NEReachability.h"

#import <Foundation/Foundation.h>

/**
 * Contains a NEReachability value which represents the reachability of the device's radios: cellular and WiFi.
 *
 * This does not guarantee that the user has network access, but rather whether
 * the device has service.
 */
NS_ASSUME_NONNULL_BEGIN
@interface NEXReachability : NEXSensorItem

-(instancetype)init NS_UNAVAILABLE;
+(instancetype)new NS_UNAVAILABLE;

@property (nonatomic, readonly) NEReachability value;

/**
 * @return `true` if the cell state is not Off or Unknown.
 */
-(bool)isCellStateOn;

/**
 * @return `true` if the cell data state is not Off or Unknown.
 */
-(bool)isCellDataStateOn;

/**
 * @return `true` if the wifi state is not Off or Unknown.
 */
-(bool)isWifiStateOn;

/**
 * Convenience getter for hasInternet() && !isSlow().
 *
 * @return `true` if Internet connection exists, and is suitable for most applications,
 *          e.g. browsing the web or downloading images.
 */
-(bool)isGood;

/**
 * @return `true` if an active and working Internet connection exists.
 */
-(bool)hasInternet;

/**
 * @return `true` if Internet connection has a slow data rate,
 *          i.e. only suitable for text/email transfers.
 */
-(bool)isSlow;

/**
 * @return `true` if Internet connection is limited or billed by usage.
 *          Data usage should be light and infrequent when this flag is true.
 */
-(bool)isMetered;

@end
NS_ASSUME_NONNULL_END
