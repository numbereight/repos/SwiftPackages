//
//  NEXDouble.h
//  NumberEightCompiled
//
//  Created by Matthew Paletta on 2021-05-20.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//
#import "NEXSensorItem.h"
#include "NEDouble.h"

NS_ASSUME_NONNULL_BEGIN

@interface NEXDouble : NEXSensorItem

-(instancetype)init NS_UNAVAILABLE;
+(instancetype)new NS_UNAVAILABLE;

@property (nonatomic, readonly) NEDouble value;

@end

NS_ASSUME_NONNULL_END
