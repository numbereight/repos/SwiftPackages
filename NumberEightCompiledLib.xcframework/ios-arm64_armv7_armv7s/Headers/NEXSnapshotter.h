//
//  NEXSnapshotter.h
//  NumberEightCompiled
//
//  Created by Matthew Paletta on 2021-05-14.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

#pragma once

#import "NEXBaseFormatter.h"
#import "NEXSnapshot.h"
#import "NEXGlimpse.h"
#import "NEXSnapshotPair.h"
#import "NEXParameters.h"

#import <Foundation/Foundation.h>

typedef NEXSnapshotPair* _Nullable (^NEXSnapshotGlimpseRuleHandler)(NEXGlimpse<__kindof NEXSensorItem*>* _Nonnull glimpse);

typedef NEXSnapshotPair* _Nullable (^NEXSnapshotSensorItemRuleHandler)(NSString* _Nonnull topic, NEXSensorItem* _Nonnull sensorItem);


///
/// @brief Groups a list of topics to be atomically queried on demand.
///
/// @discussion Instances of `NEXSnapshotter` capture a list of topics and filters, which can then be used to create snapshots.
/// A snapshot is an immutable reference of the latest events of the captured topics at that particular moment in time.
///
/// Internally, NEXSnapshotter maintains a list of subscriptions to the captured topics, with the approprate filters and updates the latest state as new events come in.
///
/// Once a snapshot is created, you can access the string representation of the topics directly, or pass it to a formatter to get a textual representation of the snapshot.
///
/// Additionally, the caller is able to change the textual representation of an individual topic if they wish by adding rules.  The appropriate rules are invoked when the NEXSnapshotter is creating the snapshot.  If no rule is provided, it will use the default description of the object.
///
/// @see `NEXBaseFormatter`
///
/// @code
/// NEXSnapshotter* snapshotter = [[NEXSnapshotter alloc] initWithDefaults];
/// [snapshotter addRuleForTopic:kNETopicActivity handler:^(NEXGlimpse<NEXActivity*>* _Nonnull activity) {
///     // For this example, we are only interested in the activity state.
///     return [[NEXSnapshotPair alloc] initWithKey:glimpse.topic values:[NEXActivity stringFromState:activity.value.state]];
/// }];
///
/// // ...
///
/// NEXSnapshot* snapshot = [snapshotter takeSnapshot];
///
/// // ...
///
/// NEXJSONFormatter* jsonFormatter = [[NEXJSONFormatter alloc] init];
/// NSString* jsonString = [jsonFormatter stringFromSnapshot:snapshot];
///
/// // ...
///
/// NEXQueryStringFormatter* qsFormatter = [[NEXQueryStringFormatter alloc] init];
/// NSString* queryString = [qsFormatter stringFromSnapshot:snapshot];
///
NS_SWIFT_NAME(Snapshotter)
@interface NEXSnapshotter : NSObject

///
/// The set of topics to include in the snapshot.
///
@property (atomic, readwrite, strong) NSSet<NSString*>* _Nonnull topics;

///
/// The mapping of topics to filters to apply when capturing data for the snapshot.
///
@property (atomic, readwrite, strong) NSDictionary<NSString*, NSString*>* _Nonnull filters;

///
/// List of default topics captured in the snapshot.
///
/// A default set of common high-level context topics that NumberEight provides.
/// Includes Activity, Device Position, Indoor/Outdoor, Place, Reachability, Situation, Time, and Weather.
///
/// @return The default list of topics used for snapshots.
///
+(NSSet<NSString*>* _Nonnull)defaultTopics;

///
/// A mapping of default filters for data captured in the snapshot.
///
/// A default mapping of common high-level context topics and filters that NumberEight provides.
/// Includes Activity, Device Position, Indoor/Outdoor, Place, Reachability, Situation, Time, and Weather.
///
/// @return The default dictionary of filters used for snapshots.
///
+(NSDictionary<NSString*, NSString*>* _Nonnull)defaultFilters;

///
/// Creates a new snapshotter with the default topics and filters.
///
/// @see `NEXSnapshotter.defaultTopics`
///
/// This constructor should be suitable for most cases.
///
/// @return A reference to the newly created NEXSnapshotter
///
-(instancetype _Nonnull)init;

///
/// Creates a new snapshotter with the default topics and filters.
///
/// @see `NEXSnapshotter.defaultTopics`
/// @see `NEXSnapshotter.defaultFilters`
///
/// This constructor should be suitable for most cases.
///
/// @return A reference to the newly created NEXSnapshotter
///
-(instancetype _Nonnull)initWithDefaults NS_REFINED_FOR_SWIFT;

///
/// Creates a new snapshotter from a list of topics and filters.
///
/// @param newTopics the set of topics to include in the snapshot
/// @param newFilters the mapping of topics to filters used when collecting data for snapshots.
///
/// @discussion The topics and filters may be adjusted at any time using the accessor methods.
///
/// @return A reference to the newly created NEXSnapshotter
///
-(instancetype _Nonnull)initWithTopics:(NSSet<NSString*>* _Nonnull)newTopics filters:(NSDictionary<NSString*, NSString*>* _Nullable)newFilters;

///
/// Creates a new snapshotter from a list of topics and `NEXParameters`.
///
/// @param newTopics the set of topics to include in the snapshot
/// @param parameters the mapping of topics to parameters used when collecting data for snapshots.
///
/// @discussion The topics and parameters may be adjusted at any time using the accessor methods.
///
/// @return A reference to the newly created NEXSnapshotter
///
-(instancetype _Nonnull)initWithTopics:(NSSet<NSString*>* _Nonnull)newTopics parameters:(NSDictionary<NSString*, NEXParameters*>* _Nullable)parameters;

///
/// Creates a new snapshotter from a set of topics. The snapshotter will assume no filters.
///
/// @param newTopics the set of topics to include in the snapshot
///
/// @return A reference to the newly created NEXSnapshotter
///
-(instancetype _Nonnull)initWithTopics:(NSSet<NSString*>* _Nonnull)newTopics;

///
/// Pauses all subscriptions in the snapshotter. This can be used to save power.
///
/// @note This function is thread safe.
///
/// @note If the snapshotter is already paused, this does nothing.
///
-(void)pause;

///
/// Resumes all subscriptions in the snapshotter paused by `[NEXSnapshotter pause]`.
///
/// @note All topics, filters, and handlers set during the object's lifetime will be used.
///
/// @note This function is thread safe.
///
/// @note If the snapshotter is already running, this cancels and restarts the subscriptions.
///
-(void)resume;

///
/// Takes a snapshot of the current state of the subscribed topics.
///
/// @note This function is thread safe.
///
/// @return An immutable snapshot of the subscribed topics at the moment the function was called.
///
-(NEXSnapshot* _Nonnull)takeSnapshot;

///
/// Takes a snapshot of the current state of the subscribed topics, after being 'translated' by the handler.
///
/// @note This function is thread safe.
///
/// @return An immutable snapshot of the subscribed topics at the moment the function was called.
///
-(NEXSnapshot* _Nonnull)takeSnapshotWithBlock:(NEXSnapshotGlimpseRuleHandler _Nullable)handler NS_REFINED_FOR_SWIFT;

@end
