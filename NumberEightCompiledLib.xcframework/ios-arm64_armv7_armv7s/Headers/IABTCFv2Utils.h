#ifndef IABTCFv2Utils_h
#define IABTCFv2Utils_h

#if __has_include("../../types/ctypes/NETypeUtils.h")
#include "../../types/ctypes/NETypeUtils.h"
#else
#include "NETypeUtils.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

NE_ENUM(uint32_t, IABTCFv2Purpose) {
    IABTCFv2PurposeStorage = 1,
    IABTCFv2PurposeBasicAds = 2,
    IABTCFv2PurposePersonalizedAds = 3,
    IABTCFv2PurposeSelectAds = 4,
    IABTCFv2PurposePersonalizedContent = 5,
    IABTCFv2PurposeSelectContent = 6,
    IABTCFv2PurposeMeasureAds = 7,
    IABTCFv2PurposeMeasureContent = 8,
    IABTCFv2PurposeMarketResearch = 9,
    IABTCFv2PurposeImproveProducts = 10
};

NE_ENUM(uint32_t, IABTCFv2SpecialFeature) {
    IABTCFv2SpecialFeatureGeolocation = 1,
    IABTCFv2SpecialFeatureScanCharacteristics = 2
};

#ifdef __cplusplus
}
#endif

#endif
