//
//  NEXSnapshotter_priv.h
//  NumberEightCompiled
//
//  Created by Matthew Paletta on 2021-10-26.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

#pragma once

#import "NEXSnapshotter.h"
#import "NEXNumberEight.h"

@interface NEXSnapshotter(Private)

///
/// Creates a new snapshotter from a list of topics and filters.
///
/// @param newTopics the set of topics to include in the snapshot
/// @param newFilters the mapping of topics to filters used when collecting data for snapshots.
///
/// @discussion The topics and filters may be adjusted at any time using the accessor methods.
///
/// @return A reference to the newly created NEXSnapshotter
///
-(instancetype _Nonnull)initWithTopics:(NSSet<NSString*>* _Nonnull)newTopics filters:(NSDictionary<NSString*, NSString*>* _Nullable)newFilters;

///
/// Specifies a rule for how the `NEXGlimpse` should be serialized to a string in the output snapshot.
///
/// @param topic The topic the rule should be applied to.  This topic should be included in `[NEXSnapshotter initWithTopics:filters]`.  If the topic is not found, or no data is published on that topic (or any sub-topics), the handler will not be called.
///
/// @param handler The function that is called to serialize the latest `NEXGlimpse` for the topic to a string.  This function should assume it takes in an `NEXGlimpse`, and returns the pair containing the key and value, as a string representation of that object in a `NEXSnapshot`.  This should return nil as the value if the object cannot be serialized.
///
/// @note Objects where the handler returns nil, or no handler is provided, will use the default `description` field of the object as the value, and the topic as the key.
///
/// @return A reference to self, used for chaining
///
-(instancetype _Nonnull)addRuleForTopic:(NSString* _Nonnull)topic handler:(NEXSnapshotGlimpseRuleHandler _Nullable)handler NS_REFINED_FOR_SWIFT;

///
/// Specifies a rule for how the `NEXSensorItem` should be serialized to a string in the output snapshot.
///
/// @param topic The topic the rule should be applied to.  This topic should be included in `[NEXSnapshotter initWithTopics:filters]`.  If the topic is not found, or no data is published on that topic (or any sub-topics), the handler will not be called.
///
/// @param handler The function that is called to serialize the most probable `NEXSensorItem` for the topic to a string.  This function should assume it takes in an `NEXSensorItem`, and returns the desired string representation of that object.  This should return nil if the object cannot be serialized.
///
/// @note Objects where the handler returns nil, or no handler is provided, will use the default `description` field of the object as the value, and the topic as the key.
///
/// @return A reference to self, used for chaining
///
-(instancetype _Nonnull)addRuleForMostProbableItemForTopic:(NSString* _Nonnull)topic handler:(NEXSnapshotSensorItemRuleHandler _Nullable)handler NS_REFINED_FOR_SWIFT;

@end
