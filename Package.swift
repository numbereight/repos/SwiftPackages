// swift-tools-version:5.3
import PackageDescription

let package = Package(
    name: "NumberEight",
    platforms: [
        .iOS(.v9)
    ],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(name: "Audiences", targets: ["Audiences", "AudiencesObjc", "NECommonObjc"]),
        .library(name: "Insights", targets: ["Insights", "InsightsObjc", "NECommonObjc"]),
        .library(name: "NumberEight", targets: ["NumberEight", "NumberEightCompiled"]),
    ],
    dependencies: [
        .package(url: "https://github.com/stephencelis/SQLite.swift", from: "0.13.0"),
        .package(url: "https://github.com/yonaskolb/Codability", from: "0.2.1")
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(name: "Audiences", dependencies: [
            .target(name: "AudiencesInternal"),
            .target(name: "AudiencesObjc"),
            .target(name: "NECommonObjc"),
            .target(name: "NumberEight"),
        ]),
        .target(name: "AudiencesObjc", dependencies: [
            .target(name: "AudiencesInternal"),
            .target(name: "Insights"),
        ], cxxSettings: [
            .unsafeFlags(["-fmodules", "-fcxx-modules"]),
        ]),
        .target(name: "AudiencesInternal", dependencies: [
            .target(name: "Insights"),
            .target(name: "InsightsInternal"),
            .target(name: "NECommonObjc"),
            .target(name: "NumberEight"),
            .product(name: "SQLite", package: "SQLite.swift"),
        ]),
        .target(name: "Insights", dependencies: [
            .target(name: "InsightsObjc"),
            .target(name: "NECommonObjc"),
            .target(name: "NumberEight"),
        ]),
        .target(name: "InsightsInternal", dependencies: [
            .target(name: "NECommonObjc"),
            .target(name: "NumberEight"),
            .product(name: "SQLite", package: "SQLite.swift"),
            .product(name: "Codability", package: "Codability"),
        ]),
        .target(name: "InsightsObjc", dependencies: [
            .target(name: "InsightsInternal"),
            .target(name: "NECommonObjc"),
            .target(name: "NumberEight"),
        ], cxxSettings: [
            .unsafeFlags(["-fmodules", "-fcxx-modules"]),
        ]),
        .target(name: "NECommonObjc"),
        .target(
            name: "NumberEight",
            dependencies: [
                .target(name: "NumberEightCompiled"),
            ]
        ),
        .binaryTarget(
            name: "NumberEightCompiled",
            path: "./NumberEightCompiled.xcframework"
        )
    ],
    cxxLanguageStandard: .cxx14
)
