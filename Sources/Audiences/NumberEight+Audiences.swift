//
//  File.swift
//  
//
//  Created by Chris Watts on 05/09/2024.
//

import NumberEight
import AudiencesObjc

// MARK: - Helper extensions -
public extension NumberEight {
    class var `Audiences`: Audiences.Type {
        typealias NEAudiences = Audiences
        return NEAudiences.self
    }
}
