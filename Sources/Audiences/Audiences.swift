//
//  Audiences+Public.swift
//  Audiences
//
//  Created by Matthew Paletta on 2021-04-28.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation
import NumberEight
import AudiencesInternal
@_exported import NECommonObjc
@_exported import AudiencesObjc

// This extension contains the Swift refined of the base Objective-C interface.
public extension Audiences {

    /**
     The list of audience memberships detected by NumberEight Audiences.
     This list changes periodically as the user uses the device more.

     - Tag: Audiences.currentMemberships

     - Returns: A set of audience memberships for the user containing an ID, audience name, and equivalent IAB Audience Taxonomy IDs.
     e.g.
     - Membership("NE-1-1", "Joggers", [ IABAudience("410") ])
     - Membership("NE-2-6", "Culture Vultures", [ IABAudience("779") ])
     - Membership("NE-2-3", "Cinema Buffs", [ IABAudience("467"), IABAudience("787", [ "PIFI3" ]) ])
     */
    static var currentMemberships: Set<Membership> {
        return Set(Audiences.__currentMemberships)
    }

    /**
     The list of audience IDs detected by NumberEight Audiences.
     This is a convenience function to return only the IDs from the list of memberships.

     - Tag: Audiences.currentIds

     - Returns: A set of audience membership IDs for the user.
                e.g.
                 - "NE-1-1"
                 - "NE-2-6"
                 - "NE-2-3"
     */
    static var currentIds: Set<String> {
        return Set(Audiences.__currentIds)
    }

    /**
     The list of audience IDs and their corresponding liveness detected by NumberEight Audiences.
     This is a convenience function to return only the extended IDs from the list of memberships.

     This is useful for use cases such as adding audience memberships to ad requests.

     - Returns: A set of extended audience membership IDs for the user.
                e.g.
                 - "NE-1-1|H"
                 - "NE-100-1|L"
                 - "NE-101-2|T"
     */
    static func currentExtendedIds() -> Set<String> {
        return Set(Audiences.__currentExtendedIds())
    }

    /**
      The list of IAB Audience Taxonomy IDs detected by NumberEight Audiences.
      This is a convenience function to return only the IAB IDs from the list of memberships.

      This is useful for use cases such as adding audience memberships to ad requests.

      - Returns: A set of IAB IDs for the user.
                 e.g.
                  - "408"
                  - "762"
     */
    static func currentIabIds() -> Set<String> {
        return Set(Audiences.__currentIabIds())
    }

    /**
      The list of extended IAB Audience Taxonomy IDs detected by NumberEight Audiences.
      This is a convenience function to return the IAB IDs along with any extensions such
      as purchase intent from the list of memberships.

      This format is as described by the IAB Seller-Defined Audiences guidance.

      This is useful for use cases such as adding audience memberships to ad requests.

      - Returns: A set of IAB IDs for the user.
                 e.g.
                  - "408|PIFI1"
                  - "762|PIFI2|PIPV2"
     */
    static func currentExtendedIabIds() -> Set<String> {
        return Set(Audiences.__currentExtendedIabIds())
    }

    /// Returns the predicted year of birth for the current user, if available.
    /// This will immediately return the current value and will fetch the new value in the background
    /// if it has not already been fetched.
    ///
    /// This is thread safe.
    ///
    /// e.g. 2023
    static func yearOfBirth() -> Int? {
        return NumberEight.estimatedYearOfBirth()
    }

    /// Asynchronously returns the predicted year of birth for the current user, if available.
    /// This will force refresh the value from the server.
    ///
    /// If multiple threads try and make a request while one is already underway, only the one
    /// request will be made, and all threads will get the same response.
    /// This is thread safe.
    ///
    /// e.g. 2023
    static func yearOfBirth(completion: @escaping (Int?) -> Void) {
        return NumberEight.estimatedYearOfBirth(completion: completion)
    }

    /**
     * Starts recording device usage to categorise the user into audiences.
     *
     * This will associate an ID with the current device. If NumberEight Insights is in use
     * with a suitable device_id, then that will be used. Otherwise, a new UUID will be created
     * and stored locally.
     *
     * Note: due to this ID, this data will be treated as Personally Identifiable Information
     * until future releases remove the need for an ID.
     * - Tag: Audiences.startRecording
     *
     * - Parameter apiToken: Token received from NumberEight.start()
     * - Parameter parameters: An optional argument which maps a list of topics to `NEXParameters`. This is
                               used internally to make subscriptions to those topics, which are then used
                               or determining live and post-audiences. The default value should be
                               suitable for most use cases.
     * - Parameter onStart: Optional callback for determining whether recording started successfully.
     */
    static func startRecording(apiToken: APIToken, parameters: [String: Parameters] = Audiences.defaultParameters, onStart: ((Result<Void, Error>) -> Void)? = nil) {
        AudiencesInternal.instance._startRecording(apiToken: apiToken, parameters: parameters, onStart: onStart)
    }
}
