//
//  Insights.h
//  Insights
//
//  Created by Oliver Kocsis on 09/10/2019.
//  Copyright © 2019 NumberEight Technologies Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for Insights.
FOUNDATION_EXPORT double NECommonObjcVersionNumber;

//! Project version string for Insights.
FOUNDATION_EXPORT const unsigned char NECommonObjcVersionString[];

#import <NEXIABAudience.h>
#import <NEXLivenessState.h>
#import <NEXMembership.h>
#import <NEXRecordingConfig.h>
