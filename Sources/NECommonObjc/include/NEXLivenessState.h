//
//  NEXLiveness.h
//  Insights
//
//  Created by Matthew Paletta on 2021-10-26.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, NEXLivenessState) {
    kNEXLivenessStateLive,
    kNEXLivenessStateHappenedToday,
    kNEXLivenessStateHappenedThisWeek,
    kNEXLivenessStateHappenedThisMonth,
    kNEXLivenessStateHabitual
};
