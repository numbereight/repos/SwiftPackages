//
//  AudienceLiveness.swift
//  Audiences
//
//  Created by Matthew Paletta on 2021-11-25.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation
import SQLite
import Insights
import InsightsInternal

///
/// - Tag: LiveAudiences
///
class LiveAudiences {
    private static let LOG_TAG = "LiveAudiences"
    private static let POST_LIVE_AUDIENCE_MAX_AGE = 30

    ///
    /// NumberEight provided default `Parameters`, suitable for most use cases.
    ///
    /// - Tag: LiveAudiences.defaultParameters
    ///
    static let defaultParameters = { () -> [String: Parameters] in
        var audiences = RecordingConfig.default.filters.mapValues { Parameters.withFilter(filter: $0) }
        audiences[kNETopicHeadphones] = Parameters.sensitivityRealTime
        audiences[kNETopicHeadphonesWired] = Parameters.sensitivityRealTime
        audiences[kNETopicHeadphonesBluetooth] = Parameters.sensitivityRealTime
        audiences[kNETopicUserMovement] = Parameters.withFilter(filter: "movavg:10s|uniq")
        audiences[kNETopicConnectionPerformance] = Parameters.withFilter(filter: "hyst:3s|uniq")
        audiences[kNETopicLockStatus] = Parameters.sensitivityRealTime
        audiences[kNETopicDeviceMovement] = Parameters.withFilter(filter: "movavg:10s|uniq")
        audiences[kNETopicVolumeLevelCategory] = Parameters.withFilter(filter: "uniq")
        return audiences
    }()

    ///
    /// Clears all historical audience observations from the database.
    ///
    /// - Tag: LiveAudiences.performDataRemoval
    ///
    static func performDataRemoval() {
        _ = LiveAudiencesDBHelper().clearAllData()
    }

    private var parameters: [String: Parameters] = [:]

    private var state = [String: LatestEventState]()
    private let ne = NumberEight()
    private let dbHelper = LiveAudiencesDBHelper()

    private let recorderMutex = PThreadMutex(type: .normal)
    private var isRunning = false

    ///
    /// Retrieves the set of audience IDs that correspond with a given `state`.
    ///
    /// - Parameter state: A map of topic to [LatestEventState](x-source-tag://LatestEventState) which contains the value and the
    ///                    confidence of the most probable value, from the `Glimpse`for that topic.
    ///
    /// - Note: Callers should call [getCurrentAudienceIds](x-source-tag://LiveAudiences.getCurrentAudienceIDs) instead of this function directly.
    ///
    /// - Note: This function is idempotent and thread-safe.
    ///
    /// - Returns: The set of audience IDs that correspond with a given `state`, or the empty set if
    ///            there are no audience IDs that match.
    ///
    /// - Tag: LiveAudiences.getAudienceIdsFromState
    ///
    internal func getAudienceIdsFromState(state: [String: LatestEventState]) -> Set<String> {
        var audienceIDs = Set<String>()
        for audience in kAudienceDefinitions {
            guard let stateValue = state[audience.topic] else {
                continue
            }

            if audience.value?.firstMatch(in: stateValue.value, options: [], range: NSRange(location: 0, length: stateValue.value.count)) != nil {
                audienceIDs.insert(audience.id)
            }
        }
        return audienceIDs
    }

    ///
    /// Updates the set of topics and their respective `Parameters` used for determining live and
    /// post-live audiences.
    ///
    /// - Note: This function will unsubscribe from all existing topics and
    ///         resubscribe to topics from the new list with their updated filters.
    ///
    /// - Note: If there is no change in topics or `Parameters` compared to the existing value,
    ///         no action is performed.
    ///
    /// - Parameter parameters: A map of topics to `Parameters`. This is used internally to make
    ///                         subscriptions to those topics, which are then used for determining live and
    ///                         post-live audiences. To revert back to the NumberEight provided defaults,
    ///                         use [defaultParameters](x-source-tag://LiveAudiences.defaultParameters).
    ///
    /// - Tag: LiveAudiences.updateTopics
    ///
    func updateTopics(parameters: [String: Parameters]) {
        if parameters == self.parameters {
            return
        }

        self.ne.unsubscribeFromAll()

        self.subscribeToTopics(parameters: parameters)
        self.parameters = parameters
    }

    ///
    /// Starts the Live Audiences recorder with new parameters.
    ///
    /// - Parameter parameters: A map of topics to `Parameters`. This is used internally to make
    ///                         subscriptions to those topics, which are then used for determining live and
    ///                         post-live audiences. To revert back to the NumberEight provided defaults,
    ///                         use [defaultParameters](x-source-tag://LiveAudiences.defaultParameters).
    /// - Tag: LiveAudiences.start
    ///
    func start(parameters: [String: Parameters]) {
        if self.isRunning {
            return
        }

        self.isRunning = true
        self.updateTopics(parameters: parameters)
    }

    ///
    /// Stops the Live Audiences recorder. No new data will be recorded while stopped.
    ///
    /// - Tag: LiveAudiences.stop
    ///
    func stop() {
        self.ne.unsubscribeFromAll()
        self.isRunning = false
    }

    ///
    /// Creates subscriptions to all topics, defined by the keys in the `parameters` map, with
    /// their corresponding `Parameters`. If `LiveAudiences` is not running, this does nothing.
    ///
    /// - Parameter parameters: A map of topics to `Parameters` used to create subscriptions.
    ///
    /// - Tag: LiveAudiences.subscribeToTopics
    ///
    private func subscribeToTopics(parameters: [String: Parameters]) {
        if !self.isRunning {
            return
        }

        for (topic, param) in parameters {
            self.ne.subscribe(to: topic, parameters: param) { [weak self] glimpse in
                // Move this to a new function so it is unit-testable without the engine.
                self?.insertNewAudienceIdsWithNewGlimpse(topic: topic, glimpse: glimpse)
            }
        }
    }

    ///
    /// Updates the current state of glimpses, and inserts or updates any new audience IDs into the
    /// database.
    ///
    /// - Parameter topic: The topic the `glimpse` was retrieved on.
    /// - Parameter glimpse: The new `Glimpse` retrieved from the NumberEight `Engine`.
    ///
    /// - Tag: LiveAudiences.insertNewAudienceIdsWithNewGlimpse
    ///
    internal func insertNewAudienceIdsWithNewGlimpse(topic: String, glimpse: Glimpse<NEXSensorItem>) {
        let audienceIds = self.getAudienceIDsWithNewGlimpse(topic: topic, glimpse: glimpse)
        // This can happen outside of a synchronized block because transactions.
        self.insertLiveAudiences(audienceIds: audienceIds, date: Date())
    }

    ///
    /// Updates the current `Glimpse` state stored in the class, and returns the set of all
    /// audience IDs that correspond with the updated `Glimpse` state.
    ///
    /// - Note: This function is thread-safe.
    ///
    /// - Parameter topic: The topic the `glimpse` was retrieved on.
    /// - Parameter glimpse: The new `Glimpse` retrieved from the NumberEight `Engine` on the `topic`.
    ///
    /// - Returns: The set of audience IDs that correspond with the updated `Glimpse` state. If no
    ///            audience IDs match, the empty set is returned.
    ///
    /// - Tag: LiveAudiences.getAudienceIDsWithNewGlimpse
    ///
    internal func getAudienceIDsWithNewGlimpse(topic: String, glimpse: Glimpse<NEXSensorItem>) -> Set<String> {
        // This is synchronized, so we don't have to clone 'this.state'.
        return self.recorderMutex.sync { () -> Set<String> in
            // Use the original topic not the glimpse topic, as that is what the rule expects
            // in getAudienceIDsFromState.
            guard let serialized = glimpse.mostProbable.serialize() else { return Set() }
            self.state[topic] = LatestEventState(value: serialized, confidence: glimpse.mostProbableConfidence)
            return self.getAudienceIdsFromState(state: self.state)
        }
    }

    ///
    /// Retrieves all current live and post-live audience memberships.
    ///
    /// - Returns: A merged set of audience memberships from live and post-live audiences.
    ///
    /// - Tag: LiveAudiences.retrieveAudiences
    ///
    func retrieveAudiences() -> Set<Membership> {
        let postLiveContextAudiences = self.retrievePostLiveAudiences(date: Date())
        let currentLiveAudienceIDs = self.getCurrentAudienceIDs()
        return self.mergeMemberships(currentPostLiveAudiences: postLiveContextAudiences, currentLiveAudienceIDs: currentLiveAudienceIDs)
    }

    ///
    /// Retrieves the set of live audience IDs that match the current `Glimpse` state.
    ///
    /// - Note: This function is thread-safe.
    ///
    /// - Returns: The set of live audience IDs that match the current `Glimpse` state.
    ///
    /// - Tag: LiveAudiences.getCurrentAudienceIDs
    ///
    private func getCurrentAudienceIDs() -> Set<String> {
        // Make a copy of the state here, and then always reference the copy in this function.
        let currentState = self.recorderMutex.sync {
            return self.state.copy()
        }
        return self.getAudienceIdsFromState(state: currentState)
    }

    ///
    /// Deduplicates, and returns the merged set of live and post-live audience `Membership`s
    /// the user is current in.
    ///
    /// - Parameter currentPostLiveAudiences: The list of active post-live audience memberships,
    ///                                       each represented by a [TimeContextAudience](x-source-tag://TimeContextAudience).
    /// - Parameter currentLiveAudienceIDs: The set of live audience IDs for the user.
    ///
    /// - Returns: The merged set of audience `Membership` from live and post-live audience memberships.
    ///
    /// - Tag: LiveAudiences.mergeMemberships
    ///
    private func mergeMemberships(currentPostLiveAudiences: [TimeContextAudience], currentLiveAudienceIDs: Set<String>) -> Set<Membership> {
        var mergedMemberships = Set<Membership>()
        for postLiveAudience in currentPostLiveAudiences {
            guard let membership = postLiveAudience.toMembership() else { continue
            }
            let mergedLiveness = { () -> NEXLivenessState in
                if postLiveAudience.age == 0 && currentLiveAudienceIDs.contains(membership.id) {
                    // Today or Live
                    return .live
                } else {
                    return membership.liveness
                }
            }()

            // No need to deduplicate because the membership will only be stored once in the database,
            // so it will only fall into one liveness category.
            mergedMemberships.insert(Membership(id: membership.id, name: membership.name,
                                             iabIds: membership.iabIds, liveness: mergedLiveness))
        }

        // Need to add all live audiences that do not have a post-live audience counterpart.
        for audienceID in currentLiveAudienceIDs {
            guard let audience = TimeContextAudience(id: audienceID, age: 0).toMembership() else { continue }
            mergedMemberships.insert(Membership(id: audience.id, name: audience.name, iabIds: audience.iabIds, liveness: .live))
        }

        return mergedMemberships
    }

    ///
    /// Given a date, retrieves all post-live audiences within 30 days of `date`.
    ///
    /// - Note: This function will also delete any audience memberships older than 30 days, relative
    ///         to `date`.
    ///
    /// - Note: This function is thread-safe.
    ///
    /// - Parameter date: The date that corresponds to `today` and defaults to the current date. This
    ///                   should only be overloaded for the purpose of testing.
    ///
    /// - Returns: A list of all [TimeContextAudience](x-source-tag://TimeContextAudience)s that have been observed within the last 30 days
    ///            from `date`. Any audience IDs that do not have a corresponding [TimeContextAudience](x-source-tag://TimeContextAudience),
    ///            will be skipped.
    ///
    /// - Tag: LiveAudiences.retrievePostLiveAudiences
    ///
    private func retrievePostLiveAudiences(date: Date) -> [TimeContextAudience] {
        let maxDate = AudiencesReporter.getDateStr(date: date)

        guard let db = LiveAudiencesDBHelper.connection else {
            NELog.msg(LiveAudiences.LOG_TAG, warning: "Failed to get connection while adding live audiences.")
            return []
        }

        do {
            try db.run("""
                DELETE FROM \(LiveAudiencesDBHelper.AUDIENCE_OBSERVATIONS_TABLE)
                    WHERE JULIANDAY(?1) - JULIANDAY(last_observation) > ?2;
            """, maxDate, LiveAudiences.POST_LIVE_AUDIENCE_MAX_AGE)

            let stmt = try db.prepare("""
                                      SELECT audience_id, JULIANDAY(?1) - JULIANDAY(last_observation) AS date_diff
                                      FROM \(LiveAudiencesDBHelper.AUDIENCE_OBSERVATIONS_TABLE)
            """, maxDate)
#if DEBUG
            db.trace {
                NELog.msg(LiveAudiences.LOG_TAG, verbose: "DB Trace: \($0)")
            }
#endif

            var rows = [TimeContextAudience]()
            while let row = try stmt.failableNext() {
                guard let audienceId = row[0] as? String else { continue }

                // SQLite is returning this as a double, safe cast it.
                guard let date_diff_double = row[1] as? Double else { continue }
                let date_diff = Int(date_diff_double)
                rows.append(TimeContextAudience(id: audienceId, age: date_diff))
            }
            return rows
        } catch let Result.error(message, code, statement) {
            NELog.msg(LiveAudiences.LOG_TAG, error: "An SQLite error occured while retrieving post-live audiences.",
                      metadata: ["code": code, "message": message, "statement": statement.debugDescription])
        } catch let error {
            NELog.msg(LiveAudiences.LOG_TAG, error: "Error while fetching post-live audiences.", metadata: ["error": error.localizedDescription])
        }

        return []
    }

    ///
    /// Inserts a set of post-live audience IDs into the database, updating the `last_observed` date
    /// to `date` if the user has already entered that audience ID.
    ///
    /// - Note: If [LiveAudiences](x-source-tag://LiveAudiences) is not running, this does nothing.
    ///
    /// - Parameter audienceIds: The set of live audience IDs the user is now a member of. These will
    ///                          automatically fall out of live audiences and into the correct post-live
    ///                          audience.
    /// - Parameter date: The date that corresponds to `today`, defaults to the current date. This should
    ///                   only be overloaded for the purpose of testing.
    ///
    /// - Tag: LiveAudiences.insertLiveAudiences
    ///
    internal func insertLiveAudiences(audienceIds: Set<String>, date: Date) {
        if audienceIds.isEmpty || !self.isRunning {
            return
        }

        guard let db = LiveAudiencesDBHelper.connection else {
            NELog.msg(LiveAudiences.LOG_TAG, warning: "Failed to get connection while adding live audiences.")
            return
        }
        let currentDate = AudiencesReporter.getDateStr(date: date)

        do {
            let tempTable = "\(LiveAudiencesDBHelper.AUDIENCE_OBSERVATIONS_TABLE)_temp"
            try db.transaction(.exclusive) {
                try db.run("""
                    CREATE TEMPORARY TABLE IF NOT EXISTS \(tempTable) (
                        audience_id TEXT
                    )
                """)

                let insertTmpStmt = try db.prepare("""
                    INSERT INTO \(tempTable) (audience_id) VALUES (?1);
                """)
                for audienceId in audienceIds {
                    try insertTmpStmt.run([audienceId])
                }

                // Need the where clause to resolve the ambiguity of the 'ON'.
                try db.run("""
                    INSERT OR REPLACE INTO \(LiveAudiencesDBHelper.AUDIENCE_OBSERVATIONS_TABLE)
                        (audience_id, last_observation)
                        SELECT audience_id, ?1 FROM \(tempTable)
                """, currentDate)

                try db.run("DROP TABLE \(tempTable);")
            }

#if DEBUG
            db.trace {
                NELog.msg(LiveAudiences.LOG_TAG, verbose: "DB Trace: \($0)")
            }
#endif
        } catch let Result.error(message, code, statement) {
            NELog.msg(LiveAudiences.LOG_TAG, error: "An SQLite error occured while inserting live audiences.", metadata: ["code": code, "message": message, "statement": statement.debugDescription])
            return
        } catch let error {
            NELog.msg(LiveAudiences.LOG_TAG, error: "Failed while inserting live audiences. ", metadata: ["error": error.localizedDescription])
            return
        }
    }
}
