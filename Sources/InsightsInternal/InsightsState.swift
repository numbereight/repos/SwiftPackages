//
//  InsightsState.swift
//  Insights
//
//  Created by Matthew Paletta on 2021-10-05.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation
import NumberEight

internal class InsightsState {
    typealias StateType = [String: [LatestEventState]]

    public var mostProbableOnly: Bool
    private var state: StateType

    init(mostProbableOnly: Bool) {
        self.state = StateType()
        self.mostProbableOnly = mostProbableOnly
    }

    public var currentState: StateType {
        return self.state
    }

    public func updateWithEvent<T: NEXSensorItem>(topic: String, glimpse: Glimpse<T>?) {
        guard let g = glimpse else { return }

        var values = [LatestEventState]()
        for value in g.possibilities {
            if value.confidence < 0.1 && !values.isEmpty {
                continue
            }

            guard let serialize = value.serialize() else { continue }
            values.append(LatestEventState(value: serialize, confidence: value.confidence))

            if self.mostProbableOnly {
                break
            }
        }

        self.state[topic] = values
    }

}
