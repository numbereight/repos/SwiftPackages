//
//  NEXInsights.m
//  Insights
//
//  Created by Matthew Paletta on 2021-09-06.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

#import "NEXInsights.h"

#import "NEXRecordingConfig.h"

// Xcode locally uses the bottom version, through the framework
// Cocoapods uses the top one, as it generated the header in a different
// place.  Here we use both, so it is flexible for either case.
#if __has_include("InsightsInternal-Swift.h")
#import "InsightsInternal-Swift.h"
#else
@import InsightsInternal;
#endif

@implementation NEXInsights

+ (void)startRecordingWithAPIToken:(NEXAPIToken* _Nonnull)apiToken {
    [NEXInsights startRecordingWithAPIToken:apiToken config:[NEXRecordingConfig defaultConfig]];
}

+ (void)startRecordingWithAPIToken:(NEXAPIToken* _Nonnull)apiToken
                            config:(NEXRecordingConfig* _Nonnull)config {
    [NEXInsights startRecordingWithAPIToken:apiToken config:config onStart:nil];
}

+ (void)startRecordingWithAPIToken:(NEXAPIToken* _Nonnull)apiToken
                           onStart:(NEXInsightsOnStartCallback _Nullable)onStart {
    [NEXInsights startRecordingWithAPIToken:apiToken
                                     config:[NEXRecordingConfig defaultConfig]
                                    onStart:onStart];
}

+ (void)startRecordingWithAPIToken:(NEXAPIToken* _Nonnull)apiToken
                            config:(NEXRecordingConfig* _Nonnull)config
                           onStart:(NEXInsightsOnStartCallback _Nullable)onStart {
    [InsightsInternal startRecordingWithApiToken:apiToken
                                          config:config
                                         onStart:^(BOOL isSuccess, NSError* _Nullable error) {
                                           if (onStart) {
                                               onStart(isSuccess, error);
                                           }
                                         }];
}

+ (void)startRecordingWithApiToken:(NEXAPIToken* _Nonnull)apiToken
                            config:(NEXRecordingConfig* _Nonnull)config
                         onStarted:(NEXInsightsOnStartCallback _Nullable)onStarted {
    [NEXInsights startRecordingWithAPIToken:apiToken config:config onStart:onStarted];
}

+ (void)startRecordingWithApiToken:(NEXAPIToken* _Nonnull)apiToken {
    [NEXInsights startRecordingWithAPIToken:apiToken];
}

+ (void)startRecordingWithApiToken:(NEXAPIToken* _Nonnull)apiToken
                            config:(NEXRecordingConfig* _Nonnull)config {
    [NEXInsights startRecordingWithAPIToken:apiToken config:config];
}

+ (void)startRecordingWithApiToken:(NEXAPIToken* _Nonnull)apiToken
                         onStarted:(NEXInsightsOnStartCallback _Nullable)onStarted {
    [NEXInsights startRecordingWithAPIToken:apiToken onStart:onStarted];
}

+ (bool)addMarkerWithName:(NSString* _Nonnull)name
               parameters:(NSDictionary<NSString*, id>* _Nonnull)parameters
                    error:(NSError* _Nullable* _Nullable)error {
    BOOL isSessionActive = false;
    @try {
        [InsightsInternal addMarkerWithName:name
                                 parameters:parameters
                         isSessionActivePtr:&isSessionActive
                                      error:error];
        return true;
    } @catch (id exception) {
        return false;
    }
}

+ (bool)addMarkerWithName:(NSString* _Nonnull)name error:(NSError* _Nullable* _Nullable)error {
    return [NEXInsights addMarkerWithName:name parameters:[NSDictionary new] error:error];
}

+ (BOOL)addMarker:(NSString* _Nonnull)name
            parameters:(NSDictionary<NSString*, id>* _Nonnull)parameters
    isSessionActivePtr:(BOOL*)isSessionActivePtr
                 error:(NSError* _Nullable* _Nullable)error {
    @try {
        [InsightsInternal addMarkerWithName:name
                                 parameters:parameters
                         isSessionActivePtr:isSessionActivePtr
                                      error:error];
        return true;
    } @catch (id exception) {
        return false;
    }
}

+ (BOOL)addMarker:(NSString* _Nonnull)name
    isSessionActivePtr:(BOOL* _Nullable)isSessionActivePtr
                 error:(NSError* _Nullable* _Nullable)error {
    return [NEXInsights addMarker:name
                       parameters:[NSDictionary new]
               isSessionActivePtr:isSessionActivePtr
                            error:error];
}

+ (bool)addMarker:(NSString* _Nonnull)name {
    NSError* error = nil;
    auto result = [NEXInsights addMarkerWithName:name error:&error];
    return error == nil && result;
}

+ (bool)addMarker:(NSString* _Nonnull)name
       parameters:(NSDictionary<NSString*, id>* _Nonnull)parameters {
    NSError* error = nil;
    const auto result = [NEXInsights addMarkerWithName:name parameters:parameters error:&error];
    return result && error == nil;
}

+ (void)stopRecording {
    [InsightsInternal stopRecording];
}

+ (void)pauseRecording {
    [InsightsInternal pauseRecording];
}

+ (NEXRecordingConfig* _Nullable)config {
    return [InsightsInternal config];
}

+ (void)updateConfig:(NEXRecordingConfig* _Nonnull)config {
    [InsightsInternal updateConfigWithConfig:config];
}

+ (BOOL)isRecording {
    return InsightsInternal.isRecording;
}

+ (BOOL)shouldPrintErrors {
    return InsightsInternal.shouldPrintErrors;
}

+ (void)setShouldPrintErrors:(BOOL)shouldPrintErrors {
    InsightsInternal.shouldPrintErrors = shouldPrintErrors;
}

@end
