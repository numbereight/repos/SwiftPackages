//
//  Insights.h
//  Insights
//
//  Created by Oliver Kocsis on 09/10/2019.
//  Copyright © 2019 NumberEight Technologies Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for Insights.
FOUNDATION_EXPORT double InsightsObjcVersionNumber;

//! Project version string for Insights.
FOUNDATION_EXPORT const unsigned char InsightsObjcVersionString[];

#import <NEXIABAudience.h>
#import <NEXInsights.h>
#import <NEXLivenessState.h>
#import <NEXMembership.h>
