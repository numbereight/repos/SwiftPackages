//
//  File.swift
//  
//
//  Created by Chris Watts on 05/09/2024.
//

import NumberEight
import InsightsObjc

// MARK: - Helper extensions -
public extension NumberEight {
    class var `Insights`: Insights.Type {
        typealias NEInsights = Insights
        return NEInsights.self
    }
}
