//
//  Snapshotter.swift
//  NumberEight
//
//  Created by Matthew Paletta on 2021-05-17.
//  Copyright © 2021 Numero Eight Ltd. All rights reserved.
//

import Foundation
import NumberEightCompiled

public extension Snapshotter {

    ///
    /// Creates a new snapshotter with the default topics and filters.
    ///
    /// This constructor should be suitable for most cases.
    ///
    /// - Returns: A reference to the newly created NEXSnapshotter
    ///
    static func initWithDefaults() -> Snapshotter {
        return Snapshotter()
    }

    ///
    /// Takes a snapshot of the current state of the subscribed topics, after being 'translated' by the handler.
    ///
    /// - Note this function is thread safe.
    ///
    /// - Returns An immutable snapshot of the subscribed topics at the moment the function was called.
    ///
    func takeSnapshot<T: NEXSensorItem>(_ callback: @escaping (_ glimpse: Glimpse<T>) -> (String?, Any)? ) -> Snapshot {
        return self.__takeSnapshot { (_ glimpse: Any) -> SnapshotPair? in
            return self.convertGlimpseToPair(glimpse: glimpse, callback: callback)
        }
    }

    private func convertGlimpseToPair<T: NEXSensorItem>(glimpse: Any, callback: @escaping (_ glimpse: Glimpse<T>) -> (String?, Any)?) -> SnapshotPair? {
        guard let itemCasted = glimpse as? Glimpse<T> else { return nil }
        guard let value = callback(itemCasted) else { return nil }
        return SnapshotPair(key: value.0, value: value.1)
    }
}
