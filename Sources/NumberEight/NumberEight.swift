//
//  NumberEight.swift
//  NumberEight
//
//  Created by Oliver Kocsis on 30/09/2019.
//  Copyright © 2019 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation
@_exported import NumberEightCompiled

public extension NumberEight {

    /**
    Subscribes a `handler` to every non-null `Glimpse` published under `topic`.

    @param topic The topic to subscribe to, e.g. "motion/accelerometer".
    A specific sensor can be subscribed to by subscribing to its `path`,
    for example: "motion/accelerometer/0".
    Subscriptions can also be more generic, such as subscribing to all motion
    events with "motion".
    The full list of topics are defined as constants, that start with `kNETopicXxxx`.
    @param parameters An optional set of `Parameters` for the subscription,
     e.g.
        `Parameters.changesOnly`
    @param handler This block is called on each new Glimpse. 
    @b This @b will @b be @b called @b from @b a @b separate @b thread.

    @return A reference to the NumberEight object to allow chaining.
    */
    @discardableResult
    func subscribe<T: NEXSensorItem>(to: String? = nil, parameters: Parameters? = nil,
                                            callback: @escaping ((_ glimpse: Glimpse<T>) -> Void))
    -> NumberEight {
        guard let topic = to ?? Engine.topicFromSensorItem(type: T.self) else {
            NELog.msg(NumberEight.LOG_TAG, warning: "Cannot determine topic for type (\(T.self.description())). Unable to subscribe.")
            return self
        }

        guard let handler = GlimpseHandler<T>(block: callback) as? GlimpseHandler<NEXSensorItem> else {
            NELog.msg(NumberEight.LOG_TAG, warning: "Cannot cast callback to GlimpseHandler<T>. Unable to subscribe to topic: \(topic)")
            return self
        }
        return self.__subscribe(toTopic: topic, parameters: parameters, handler: handler)
    }

    /**
    Subscribes a `handler` to every non-null `Glimpse` published under `topic`.

    @param topic The topic to subscribe to, e.g. "motion/accelerometer".
    A specific sensor can be subscribed to by subscribing to its `path`,
    for example: "motion/accelerometer/0".
    Subscriptions can also be more generic, such as subscribing to all motion
    events with "motion".
    The full list of topics are defined as constants, that start with `kNETopicXxxx`.
    @param parameters An optional list of `Parameters` for the subscription,
    e.g.
         `[Parameters.changesOnly]`
    @param handler This block is called on each new Glimpse. 
    @b This @b will @b be @b called @b from @b a @b separate @b thread.

    @return A reference to the NumberEight object to allow chaining.
    */
    @discardableResult
    func subscribe<T: NEXSensorItem>(to: String? = nil, parameters: [Parameters],
                                            callback: @escaping ((_ glimpse: Glimpse<T>) -> Void)) -> NumberEight {
        return self.subscribe(to: to,
                              parameters: Engine.parameterListToParameters(params: parameters),
                              callback: callback)
    }

    /**
    Requests a new `Glimpse` from a publisher publishing to `topic`.

    The `handler` will be called at most once. If no publishers are able to
    honour the request, the handler will not be called.
    If multiple publishers honour the request, only the first to publish will
    call the handler.

    @see `subscribeToTopic`
    @param topic The topic to query.
    @param parameters An optional set of `Parameters` for the subscription,
    e.g.
        `Parameters.changesOnly`
    @param handler This block is called when request is complete.
    `handler` is guaranteed to be called either once or not at all.
    @b This @b will @b be @b called @b from @b a @b separate @b thread.

    @return `false` if no requestable publishers are found.
    */
    func request<T: NEXSensorItem>(forTopic: String? = nil, parameters: Parameters? = nil,
                                          callback: @escaping ((_ glimpse: Glimpse<T>) -> Void)) -> Bool {
        guard let topic = forTopic ?? Engine.topicFromSensorItem(type: T.self) else {
            NELog.msg(NumberEight.LOG_TAG, warning: "Cannot determine topic for type (\(T.self.description())). Unable to perform request.")
            return false
        }

        guard let handler = GlimpseHandler<T>(block: callback) as? GlimpseHandler<NEXSensorItem> else {
            NELog.msg(NumberEight.LOG_TAG, warning: "Cannot cast callback to GlimpseHandler<T>. Unable to perform request for topic: \(topic)")
            return false
        }
        return self.__request(forTopic: topic, parameters: parameters, handler: handler)
    }

    /**
    Requests a new `Glimpse` from a publisher publishing to `topic`.

    The `handler` will be called at most once. If no publishers are able to
    honour the request, the handler will not be called.
    If multiple publishers honour the request, only the first to publish will
    call the handler.

    @see `subscribeToTopic`
    @param topic The topic to query.
    @param parameters An optional list of `Parameters` for the subscription,
    e.g.
        `[Parameters.changesOnly]`
    @param handler This block is called when request is complete.
    `handler` is guaranteed to be called either once or not at all.
    @b This @b will @b be @b called @b from @b a @b separate @b thread.

    @return `false` if no requestable publishers are found.
    */
    func request<T: NEXSensorItem>(forTopic: String? = nil, parameters: [Parameters],
                                          callback: @escaping ((_ glimpse: Glimpse<T>) -> Void)) -> Bool {
        return self.request(forTopic: forTopic,
                            parameters: Engine.parameterListToParameters(params: parameters),
                            callback: callback)
    }

    
    /// Returns the predicted year of birth for the current user, if available.
    /// This will immediately return the current value and will fetch the new value in the background
    /// if it has not already been fetched.
    ///
    /// This is thread safe.
    ///
    /// e.g. 2023
    static func estimatedYearOfBirth() -> Int? {
        return NumberEight.__estimatedYearOfBirth()?.intValue
    }

    
    /// Asynchronously returns the predicted year of birth for the current user, if available.
    /// This will force refresh the value from the server.
    ///
    /// If multiple threads try and make a request while one is already underway, only the one
    /// request will be made, and all threads will get the same response.
    /// This is thread safe.
    ///
    /// e.g. 2023
    static func estimatedYearOfBirth(completion: @escaping (Int?) -> Void) {
        NumberEight.__estimatedYearOfBirth { value in
            completion(value?.intValue)
        }
    }
}
