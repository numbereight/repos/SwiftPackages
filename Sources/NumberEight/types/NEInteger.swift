//
//  NEInteger.swift
//  NumberEight
//
//  Created by Matthew Paletta on 2021-01-20.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation
import NumberEightCompiled

extension NEInteger: Codable {

    public init(from decoder: Decoder) throws {
        self.init()
        let decCont = try decoder.container(keyedBy: CodingKeys.self)
        self.value = try decCont.decode(Int32.self, forKey: .value)
    }

    public func encode(to encoder: Encoder) throws {
        var encCont = encoder.container(keyedBy: CodingKeys.self)
        try encCont.encode(self.value, forKey: .value)
    }

    enum CodingKeys: String, CodingKey {
        case value
    }
}

#if compiler(>=6.0)
extension NEInteger: @retroactive Comparable {
    public static func<(inLhs: NEInteger, inRhs: NEInteger) -> Bool {
        return inLhs.value < inRhs.value
    }
}
#else
extension NEInteger: Comparable {
    public static func<(inLhs: NEInteger, inRhs: NEInteger) -> Bool {
        return inLhs.value < inRhs.value
    }
}
#endif

#if compiler(>=6.0)
extension NEInteger: @retroactive AdditiveArithmetic {
    public static var zero: NEInteger {
        return NEInteger(value: 0)
    }

    public static func+(inLhs: NEInteger, inRhs: NEInteger) -> NEInteger {
        return NEInteger(value: inLhs.value + inRhs.value)
    }

    public static func-(inLhs: NEInteger, inRhs: NEInteger) -> NEInteger {
        return NEInteger(value: inLhs.value - inRhs.value)
    }

    public static func*(inLhs: NEInteger, inRhs: NEInteger) -> NEInteger {
        return NEInteger(value: inLhs.value * inRhs.value)
    }

    public static func/(inLhs: NEInteger, inRhs: NEInteger) -> NEInteger {
        return NEInteger(value: inLhs.value / inRhs.value)
    }
}
#else
extension NEInteger: AdditiveArithmetic {
    public static var zero: NEInteger {
        return NEInteger(value: 0)
    }

    public static func+(inLhs: NEInteger, inRhs: NEInteger) -> NEInteger {
        return NEInteger(value: inLhs.value + inRhs.value)
    }

    public static func-(inLhs: NEInteger, inRhs: NEInteger) -> NEInteger {
        return NEInteger(value: inLhs.value - inRhs.value)
    }

    public static func*(inLhs: NEInteger, inRhs: NEInteger) -> NEInteger {
        return NEInteger(value: inLhs.value * inRhs.value)
    }

    public static func/(inLhs: NEInteger, inRhs: NEInteger) -> NEInteger {
        return NEInteger(value: inLhs.value / inRhs.value)
    }
}
#endif

#if compiler(>=6.0)
extension NEInteger: @retroactive Equatable {
    public static func == (inLhs: NEInteger, inRhs: NEInteger) -> Bool {
        var lhs = inLhs
        var rhs = inRhs
        return NEInteger_isEqual(&lhs, &rhs)
    }
}
#else
extension NEInteger: Equatable {
    public static func == (inLhs: NEInteger, inRhs: NEInteger) -> Bool {
        var lhs = inLhs
        var rhs = inRhs
        return NEInteger_isEqual(&lhs, &rhs)
    }
}
#endif
