//
//  NELocationCoordinate.swift
//  NumberEight
//
//  Created by Matthew Paletta on 2021-01-20.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation
import NumberEightCompiled

extension NELocationCoordinate2D: Codable {

    public init(from decoder: Decoder) throws {
        self.init()
        let decCont = try decoder.container(keyedBy: CodingKeys.self)
        self.latitude = try decCont.decode(Double.self, forKey: .latitude)
        self.longitude = try decCont.decode(Double.self, forKey: .longitude)
    }

    public func encode(to encoder: Encoder) throws {
        var encCont = encoder.container(keyedBy: CodingKeys.self)
        try encCont.encode(self.latitude, forKey: .latitude)
        try encCont.encode(self.longitude, forKey: .longitude)
    }

    enum CodingKeys: String, CodingKey {
        case latitude
        case longitude
    }
}

#if compiler(>=6.0)
extension NELocationCoordinate2D: @retroactive Equatable {
    public static func == (inLhs: NELocationCoordinate2D, inRhs: NELocationCoordinate2D) -> Bool {
        var lhs = inLhs
        var rhs = inRhs
        return NELocationCoordinate2D_isEqual(&lhs, &rhs)
    }
}
#else
extension NELocationCoordinate2D: Equatable {
    public static func == (inLhs: NELocationCoordinate2D, inRhs: NELocationCoordinate2D) -> Bool {
        var lhs = inLhs
        var rhs = inRhs
        return NELocationCoordinate2D_isEqual(&lhs, &rhs)
    }
}
#endif
