//
//  NEVector3D.swift
//  NumberEight
//
//  Created by Matthew Paletta on 2021-01-20.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation
import NumberEightCompiled

extension NEVector3D: Codable {

    public init(from decoder: Decoder) throws {
        self.init()
        let decCont = try decoder.container(keyedBy: CodingKeys.self)
        self.x = try decCont.decode(Double.self, forKey: .x)
        self.y = try decCont.decode(Double.self, forKey: .y)
        self.z = try decCont.decode(Double.self, forKey: .z)
    }

    public func encode(to encoder: Encoder) throws {
        var encCont = encoder.container(keyedBy: CodingKeys.self)

        try encCont.encode(x, forKey: .x)
        try encCont.encode(y, forKey: .y)
        try encCont.encode(z, forKey: .z)
    }

    enum CodingKeys: String, CodingKey {
        case x
        case y
        case z
    }

    public static func+(inLHS: NEVector3D, inRHS: NEVector3D) -> NEVector3D {
        return NEVector3D(x: inLHS.x + inRHS.x,
                          y: inLHS.y + inRHS.y,
                          z: inLHS.z + inRHS.z)
    }

    public static func-(inLHS: NEVector3D, inRHS: NEVector3D) -> NEVector3D {
        return NEVector3D(x: inLHS.x - inRHS.x,
                          y: inLHS.y - inRHS.y,
                          z: inLHS.z - inRHS.z)
    }
}

#if compiler(>=6.0)
extension NEVector3D: @retroactive Equatable {
    public static func == (inLhs: NEVector3D, inRhs: NEVector3D) -> Bool {
        var lhs = inLhs
        var rhs = inRhs
        return NEVector3D_isEqual(&lhs, &rhs)
    }
}
#else
extension NEVector3D: Equatable {
    public static func == (inLhs: NEVector3D, inRhs: NEVector3D) -> Bool {
        var lhs = inLhs
        var rhs = inRhs
        return NEVector3D_isEqual(&lhs, &rhs)
    }
}
#endif
