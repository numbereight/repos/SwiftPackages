//
//  NEPlaceContext.swift
//  NumberEight
//
//  Created by Matthew Paletta on 2021-01-20.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation
import NumberEightCompiled

#if compiler(>=6.0)
extension NEPlaceMajor: @retroactive LosslessStringConvertible {
    public init?(_ description: String) {
        self = description.withCString { (ptr: UnsafePointer<Int8>) -> NEPlaceMajor in
            return NEPlace_majorFromRepr(ptr)
        }
    }

    public var description: String {
        return String(cString: NEPlace_reprFromMajor(self))
    }
}
#else
extension NEPlaceMajor: LosslessStringConvertible {
    public init?(_ description: String) {
        self = description.withCString { (ptr: UnsafePointer<Int8>) -> NEPlaceMajor in
            return NEPlace_majorFromRepr(ptr)
        }
    }

    public var description: String {
        return String(cString: NEPlace_reprFromMajor(self))
    }
}
#endif

#if compiler(>=6.0)
extension NEPlaceMinor: @retroactive LosslessStringConvertible {
    public init?(_ description: String) {
        self = description.withCString { (ptr: UnsafePointer<Int8>) -> NEPlaceMinor in
            return NEPlace_minorFromRepr(ptr)
        }
    }

    public var description: String {
        return String(cString: NEPlace_reprFromMinor(self))
    }
}
#else
extension NEPlaceMinor: LosslessStringConvertible {
    public init?(_ description: String) {
        self = description.withCString { (ptr: UnsafePointer<Int8>) -> NEPlaceMinor in
            return NEPlace_minorFromRepr(ptr)
        }
    }

    public var description: String {
        return String(cString: NEPlace_reprFromMinor(self))
    }
}
#endif

#if compiler(>=6.0)
extension NEPlaceContextIndex: @retroactive LosslessStringConvertible {
    public init?(_ description: String) {
        self = description.withCString { (ptr: UnsafePointer<Int8>) -> NEPlaceContextIndex in
            return NEPlace_contextIndexFromRepr(ptr)
        }
    }

    public var description: String {
        return String(cString: NEPlace_reprFromContextIndex(self))
    }
}
#else
extension NEPlaceContextIndex: LosslessStringConvertible {
    public init?(_ description: String) {
        self = description.withCString { (ptr: UnsafePointer<Int8>) -> NEPlaceContextIndex in
            return NEPlace_contextIndexFromRepr(ptr)
        }
    }

    public var description: String {
        return String(cString: NEPlace_reprFromContextIndex(self))
    }
}
#endif

#if compiler(>=6.0)
extension NEPlaceContextKnowledge: @retroactive LosslessStringConvertible {
    public init?(_ description: String) {
        self = description.withCString { (ptr: UnsafePointer<Int8>) -> NEPlaceContextKnowledge in
            return NEPlace_contextKnowledgeFromRepr(ptr)
        }
    }

    public var description: String {
        return String(cString: NEPlace_reprFromContextKnowledge(self))
    }
}
#else
extension NEPlaceContextKnowledge: LosslessStringConvertible {
    public init?(_ description: String) {
        self = description.withCString { (ptr: UnsafePointer<Int8>) -> NEPlaceContextKnowledge in
            return NEPlace_contextKnowledgeFromRepr(ptr)
        }
    }

    public var description: String {
        return String(cString: NEPlace_reprFromContextKnowledge(self))
    }
}
#endif

extension NEPlaceContext {

    public var copyAsDictionary: [NEPlaceContextIndex: NEPlaceContextKnowledge] {
        var dict = [NEPlaceContextIndex: NEPlaceContextKnowledge]()
        for i in UInt32(0)..<UInt32(NEPlaceContext_numberOfMembers) {
            guard let idx = NEPlaceContextIndex(rawValue: i) else {
                break
            }
            var selfVar = self
            dict[idx] = NEPlaceContext_knowledgeAt(&selfVar, idx)
        }
        return dict
    }

    fileprivate var copyAsLosslessStringDictionary: [String: String] {
        var dict = [String: String]()
        for i in UInt32(0)..<UInt32(NEPlaceContext_numberOfMembers) {
            guard let idx = NEPlaceContextIndex(rawValue: i) else {
                break
            }
            var selfVar = self
            dict[idx.description] = NEPlaceContext_knowledgeAt(&selfVar, idx).description
        }
        return dict
    }
}

#if compiler(>=6.0)
extension NEPlaceContext: @retroactive Equatable {
    public static func == (inLhs: NEPlaceContext, inRhs: NEPlaceContext) -> Bool {
        var lhs = inLhs
        var rhs = inRhs
        return NEPlaceContext_isEqual(&lhs, &rhs)
    }
}
#else
extension NEPlaceContext: Equatable {
    public static func == (inLhs: NEPlaceContext, inRhs: NEPlaceContext) -> Bool {
        var lhs = inLhs
        var rhs = inRhs
        return NEPlaceContext_isEqual(&lhs, &rhs)
    }
}
#endif

extension NEPlaceContext {
    func contains(_ knowledge: NEPlaceContextKnowledge) -> Bool {
        var selfVar = self
        return NEPlaceContext_contains(&selfVar, knowledge)
    }
}

extension NEPlace: Codable {

    public init(from decoder: Decoder) throws {
        self.init()
        let decCont = try decoder.container(keyedBy: CodingKeys.self)

        let lossLessDict = try decCont.decode([String: String].self, forKey: .context)
        for (idxAsStr, knowAsStr) in lossLessDict {
            guard
                let idx = NEPlaceContextIndex(idxAsStr),
                let decodedKnowledge = NEPlaceContextKnowledge(knowAsStr)
            else {
                break
            }
            NEPlace_setContextKnowledgeAt(&self, idx, decodedKnowledge)
        }

        self.major = NEPlaceMajor(try decCont.decode(String.self, forKey: .major)) ?? .unavailable
        self.minor = NEPlaceMinor(try decCont.decode(String.self, forKey: .major)) ?? .unknown
    }

    public func encode(to encoder: Encoder) throws {
        var encCont = encoder.container(keyedBy: CodingKeys.self)

        try encCont.encode(self.context.copyAsLosslessStringDictionary, forKey: .context)
        try encCont.encode(self.major.description, forKey: .major)
        try encCont.encode(self.minor.description, forKey: .minor)
    }

    enum CodingKeys: String, CodingKey {
        case context
        case major
        case minor
    }
}

#if compiler(>=6.0)
extension NEPlace: @retroactive Equatable {
    public static func == (inLhs: NEPlace, inRhs: NEPlace) -> Bool {
        var lhs = inLhs
        var rhs = inRhs
        return NEPlace_isEqual(&lhs, &rhs)
    }
}
#else
extension NEPlace: Equatable {
    public static func == (inLhs: NEPlace, inRhs: NEPlace) -> Bool {
        var lhs = inLhs
        var rhs = inRhs
        return NEPlace_isEqual(&lhs, &rhs)
    }
}
#endif

extension NEPlace {
    func contains(contextKnowledge: NEPlaceContextKnowledge) -> Bool {
        var selfVar = self
        return NEPlace_containsContextKnowledge(&selfVar, contextKnowledge)
    }
}
